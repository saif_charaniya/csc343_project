package user_interface;

import data_managers.Commands;
import data_managers.SQLStartUp;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Time;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Scanner;

/** PizzaTerminal.java 
 * MAIN UI CLASS:
 * 	to communicate with the user of the application
 * 	
 * @author saif, Kamal
 *
 */

public class PizzaTerminal {
  
    private static Scanner sc = null;
    private static Commands commands;
    private static String error = "An error has occured. Sorry.";
  
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//start connection
		boolean connected = SQLStartUp.initialize();
		
		sc = new Scanner(System.in);
		
		//initialize commands
		if (connected) {
			commands = SQLStartUp.getCommands();
		}
		
		//start program
		onStart(connected);

	}

	
	/**
	 * Show ui on start screen
	 */
	public static void onStart(Boolean connected){
		
		if (connected) {
		
		//greeting
			System.out.println("Welcome to Pizzas R' US User Terminal!\n");
			System.out.println("Please select from the following three options:\n");
			
		//Taking input and executing.
			String input = "";
            int choice = -1;
            do {
                Menu(); //Print Menu
                input = sc.nextLine();
                try {
                    choice = Integer.parseInt(input);
                    switch (choice) { 
                    case 1:
                        Customer_Interface();
                        break;
                    case 2:
                        Store_Interface();
                        break;

                    default:
                    	System.out.println("Thank you for visiting!");
                        break;
                    }
                } catch (NumberFormatException e) {
                    input = "-1";
                }
            } while (input.compareTo("0") != 0);
			
		}
		
		else {
			
			System.out.println("Not Connected!\nPlease try again.\n");
			System.out.println("Hello");
			
		}
	}
	
	   //Print menu options
    private static void Store_Interface_Menu() {
        System.out.println("\n=========Store Interface Menu=========");
        System.out.println("0. Main Menu.");
        System.out.println("1. Add Customers.");
        System.out.println("2. Deactivate/Activate.");
        System.out.println("3. Today's Transactions.");
        System.out.println("4. Select new sale items.");
        System.out.println("5. Pause Standing order.");
        System.out.println("6. Best Sellers.");
        System.out.println("7. Delinquincy Report.");
        System.out.println("8. Sales Report");
        System.out.println("9. Remove all items on sale");
        System.out.println("10. Show all sale items");
        System.out.println("11. Worst Sellers Report");
        System.out.print("Choose one of the previous options [0-12]: ");
    }
    
    private static void Customer_Interface_Menu() {
      System.out.println("\n=========Customer Interface Menu=========");
      System.out.println("0. Main Menu");
      System.out.println("1. Place order/Revieve Request.");
      System.out.println("2. See Suggested Products");
      System.out.println("3. See Transaction History");
      System.out.println("4. See my orders");
      System.out.println("5. Search for Product");
      System.out.println("6. Update my info");
      System.out.println("7. Item on sales");
      System.out.println("8. See my balance");
  }

    private static void Menu() {
      System.out.println("=========MENU=========");
      System.out.println("0. Exit");
      System.out.println("1. Customer Interface.");
      System.out.println("2. Store Interface.");
  }
	
    private static void Customer_Interface(){
      int input = 0;
      int choice = -1;
      do {
          Customer_Interface_Menu(); //Print Menu
          System.out.print("Input: ");
          input = sc.nextInt();
          choice = input;
          try {
              switch (choice) { 
              case 1:
                Customer_Places_Order();
                  System.out.print("==================");
                  break;
              case 2:
                  Suggested_Products();
                  System.out.print("==================");
                  break;
                  
              case 3:
                All_Customer_Transactions();
                System.out.print("==================");
                break;
              
              case 4:
                Todays_Orders();
                System.out.print("==================");
                break;
              case 5:
                displayProducts();
                System.out.print("==================");
                break;
              case 6:
                System.out.print("Customer ID: ");
                int id = sc.nextInt();
                System.out.print("What do you want to change?\n1. First Name"
                    + "\n2.Last Name\n3.Gender\n4.Age\nInput: ");
                input = sc.nextInt();
                switch(input){
                  case 1:
                    System.out.print("New Value: ");
                    String value = sc.next();
                    if (commands.updateCustomer(id, value, "", "", -1) == -1) {
                    	System.out.println(error);
                    }
                    else {
                    	System.out.println("Update Completed");
                    }
                    break;
                  case 2:
                    System.out.print("New Value: ");
                    value= sc.next();
                    if (commands.updateCustomer(id, "", value, "", -1) == -1) {
                    	System.out.println(error);
                    }
                    else {
                    	System.out.println("Update Completed");
                    }
                    break;
                  case 3:
                    System.out.print("New Value: ");
                    value = sc.next();
                    if (commands.updateCustomer(id, "", "", value, -1) == -1) {
                    	System.out.println(error);
                    }
                    else {
                    	System.out.println("Update Completed");
                    }
                    break;
                  case 4:
                    System.out.print("New Value: ");
                    value = sc.next();
                    if (commands.updateCustomer(id, "", "", "",  
                    		Integer.parseInt(value)) == -1) {
                    	System.out.println(error);
                    }
                    else {
                    	System.out.println("Update Completed");
                    }
                    break;
                  default:
                    break;
                    
                 
                }
                break;
              case 7:
                Print_Items_On_Sale();
                break;
                
              case 8:
            	  printCustomerBalance();
                
              default:
                  break;
              }
              System.out.println("Enter 1 to continue");
              while(!sc.nextLine().equals("1"));
              System.out.println("===================");
          } catch (NumberFormatException e) {
              input = -1;
          }
      } while (input != 0);
    }
    
    private static void printCustomerBalance() {
		// TODO Auto-generated method stub
    	
    	System.out.println("Your customer id:");
    	int cid = sc.nextInt();
    	
    	double balance = commands.getCustomerBalance(cid);
    	System.out.printf("Your balance : %.2f", balance);
    	System.out.println();
	}


	/** get the items on sale in print them to the user **/
	private static void Print_Items_On_Sale() {
      
		ResultSet items = commands.getItemsOnSale();
		
		if (items == null) {
			System.out.println("An error occurred, sorry");
			return;
		}
		
		//print header:
		String format = "%3d%20s%20s%20.2f%20.2f";
		String headerFormat = "%3s%20s%20s%20s%20s";
		System.out.format
			(headerFormat, "ID", "Name", "Category", "Price", "Sale Price");
		System.out.println();
		
		//print all sale items
		try {
			while (items.next()) {
				int id = items.getInt(1);
				String name = items.getString(2);
				String cat = items.getString(3);
				double price = items.getDouble(4);
				double salePrice = items.getDouble(5);
				
				System.out.format(format, id, name, cat, price, salePrice);
				System.out.println();
			}
		}
		
		catch(SQLException e) {
			System.err.println(e.toString());
			return;
		}
    }


  private static void Store_Interface(){
      String input = "";
      int choice = -1;
      do {
          Store_Interface_Menu(); //Print Menu
          System.out.print("Input: ");
          input = sc.nextLine();
          try {
              choice = Integer.parseInt(input);
              switch (choice) { 
              case 1:
                  Add_Customer();
                  System.out.println("=================");
                  break;
              case 2:
                 //Deactivate/activate
                Activate_Deactivate_Customer();
                System.out.println("=================");
                break;
              case 3:
                //Today's Transaction
            	  todayTransactions();
            	
                break;    
              case 4:
            	  //select new sale item
            	  chooseNewSaleItem();
                
                break;
              case 5:
            	  //pause standing order
                
                break;
              case 6:
            	  //best sellers
            	  showBestSellers();
                
                break;
              case 7:
            	  //delinquency report
            	  showDelinquents();
                break;
              case 8:
                
                break;
              case 9:
            	  //remove all sale items
            	  removeSaleItems();
            	  
              case 10:
            	  //show all sale items
            	  showSaleItems();
            	  
              case 11:
            	  //show worst sellers
            	  showWorstSellers();
                
                break;
              default:
                  break;
              }
              System.out.println("Enter 1 to continue");
              while(!sc.nextLine().equals("1"));
          } catch (NumberFormatException e) {
              input = "-1";
          }
      } while (input.compareTo("0") != 0);
	  
	}
  
	// TODO Auto-generated method stub
	



	private static void showWorstSellers() {
	// TODO Auto-generated method stub
		commands.worstProducts("ASC");
	
}


	private static void todayTransactions() {
	// TODO Auto-generated method stub
  		
  		String today = todayDate();
  		
  		ResultSet trans = commands.todayTransaction(today);
  		
  	//print headers:
  			String format = "%20d%20s%20s%20.2f%20.2f";
  			String headerFormat = "%20s%20s%20s%20s%20s";
  			System.out.format
  				(headerFormat, "Transaction ID", "Status", "Standby", "Price", 
  						"Delivery Charge");
  			System.out.println();
  			
  			try {
  				
  				while (trans.next()) {
  					
  					int tid = trans.getInt(1);
  					String status = trans.getString(2);
  					String standby = trans.getString(3);
  					double price = trans.getDouble(4);
  					double del = trans.getDouble(5);
  					
  					System.out.format(format, tid, status, standby, price, del);
  					System.out.println();
  				}
  			}
  			
  			catch (SQLException e) {
  				System.err.println(e.toString());
  				return;
  			}
	
  	}


	/**
  	 * show best selling products
  	 */
  	private static void showBestSellers() {

  		commands.topPurchaseALL();
  	}


	private static void chooseNewSaleItem() {
	
  		//get product id
  		System.out.print("Product ID:");
        int id = sc.nextInt();
        
        //get sale price
        System.out.print("Sale Price:");
        double salePrice = sc.nextDouble();
        
        if (commands.setItemOnSale(id, salePrice) == -1){
        	System.out.println(error);
        	return;
        }
        
        System.out.println("Update completed");
  	}


	/**
  	 * show all prodcuts on sale to user
  	 */
  	private static void showSaleItems() {
	
  		//get resultset
  		System.out.println("Show items that are on sale");
		ResultSet products = commands.getItemsOnSale();
		
		if (products == null) {
			System.out.println(error);
			return;
		}
		
		//print header:
		
		//print headers:
		String format = "%20d%20s%20s%20.2f%20.2f";
		String headerFormat = "%20s%20s%20s%20s%20s";
		System.out.format
			(headerFormat, "Product ID", "Name", "Category", "Reg Price", 
					"Sale Price");
		System.out.println();
		
		try {
			
			while (products.next()) {
				
				int pid = products.getInt(1);
				String name = products.getString(2);
				String cat = products.getString(3);
				double price = products.getDouble(4);
				double salePrice = products.getDouble(5);
				
				System.out.format(format, pid, name, cat, price, salePrice);
				System.out.println();
			}
		}
		
		catch (SQLException e) {
			System.err.println(e.toString());
			return;
		}
  	}


	/**
  	 * remove all sale prices for all products 
  	 */
  	private static void removeSaleItems() {
	
  		if (commands.removeAllSaleItems() == -1) {
  			System.out.println(error);
  			return;
  		}
  		
  		System.out.println("All sales from products removed.");
  	}


	/**
  	 * 
  	 * @return string of today's date in YYYY-MM-DD format
  	 */
  	private static String todayDate () {
  		
  		Date currentTime = new Date();
  		SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd");
  		return ft.format(currentTime);
  	}

	/** get rows from customers that a delinquent and display to user */
	private static void showDelinquents() {
	
		
		String todayDate = todayDate();
		
		//get resultset
		ResultSet delinquents = commands.delinquentCustomers(todayDate);
		
		if (delinquents == null) {
			System.out.println(error);
			return;
		}
		
		//print header:
		
		//print headers:
		String format = "%20d%20s%20s%20.2f";
		String headerFormat = "%20s%20s%20s%20s";
		System.out.format
			(headerFormat, "Customer ID", "First Name", "Last Name",
					"Account Balance");
		System.out.println();
		
		try {
			
			while (delinquents.next()) {
				
				int cid = delinquents.getInt(1);
				String fname = delinquents.getString(2);
				String lname = delinquents.getString(3);
				double balance = delinquents.getDouble(4);
				
				System.out.format(format, cid, fname, lname, balance);
				System.out.println();
			}
		}
		
		catch (SQLException e) {
			System.err.println(e.toString());
			return;
		}
	}


	private static void Activate_Deactivate_Customer(){
	  System.out.print("Customer ID:");
      int id = sc.nextInt();
      
      System.out.println("Activate or deactivate:");
      System.out.println("1. activate");
      System.out.print("2. de-activate\nInput:");
      int input = sc.nextInt();
      switch(input){
        case 1:
          commands.reactivateCustomer (id);
          break;
        case 2:
          commands.deactivateCustomer (id);
          break;
        default:
           break;
      }
    
	}
	
	private static void Add_Customer(){
	  System.out.println("=========Adding Customer==========");
      System.out.print("First Name: ");
      String fname = sc.next();
      System.out.print("Last Name: ");
      String lname = sc.next();
      System.out.print("Gender: ");
      String Gender = sc.next();
      System.out.print("Age: ");
      int age = sc.nextInt();
      System.out.print("Account Balance: ");
      double AccountBalance = sc.nextDouble();
      
      int id = commands.addCustomer (fname, lname, Gender, age, AccountBalance);
      if (id == -1) {
    	  System.out.println(error);
    	  return;
      }
      
      System.out.println("=======Address=======");
      System.out.println("Country: Canada");
      System.out.println("State: Ontario");
      System.out.println("City: Toronto"); //more proffessional
      
      /*TODO: add method to select street of address */
      System.out.println("Enter steet name:");
      String street = sc.next();
      System.out.println("Street Number: ");
      int StreetNum = sc.nextInt(); 
      System.out.println("Unit Number(1-10):");
      int UnitNum = sc.nextInt();
      System.out.println("Postal Code: ");
      String PostalCode = sc.next();
      double deliveryCharge = 7;
      if (UnitNum> 5){
        deliveryCharge = 7;
         }
      else
        deliveryCharge = 5;
      
      int success = commands.createAddress(id, "Canada", "Ontario", 
          "Toronto", street, StreetNum, UnitNum, PostalCode, 
          deliveryCharge);
      
      if (success == -1) {
    	  System.out.println(error);
    	  return;
      }
      
      System.out.println("=======Payment Method=======");
      System.out.println("Choose one");
      System.out.println("1. Credit");
      System.out.println("2. Check");
      System.out.println("3. Cash");
      System.out.println("Input:");
      int input = sc.nextInt();
      String paymentMethod = "";
      
      switch(input){
        case 1:
          paymentMethod = "Credit";
          System.out.print("Credit Number:");
          String creditNum = sc.next();
          System.out.print("exp:");
          String exp = sc.next();
          System.out.print("CCV:");
          String ccv = sc.next();
          
          success = commands.addCreditPayment(id, exp, ccv, creditNum);
          
          if (success == -1) {
        	  System.out.println(error);
        	  return;
          }
          
          break;
        case 2:
          paymentMethod = "Check";
          System.out.print("Account Number:");
          String AccountNum = sc.next();
          System.out.print("Balance:");
          double balance = sc.nextDouble();
          success = commands.addChequePayment(id, AccountNum, balance);
          if (success == -1) {
        	  System.out.println(error);
        	  return;
          }
          break;
        case 3:
          paymentMethod = "Cash";
          success = commands.addCashPayment(id);
          if (success == -1) {
        	  System.out.println(error);
        	  return;
          }
          break;
      }
      
      System.out.println("Customer details added successfully");
	}
	
	private static void Customer_Places_Order(){
		  
		System.out.print("Customer ID:");
	  int id = sc.nextInt();
	  
	  //fetches all products of category pizza from database and prints
	  commands.Print_Pizza_Menu();
	  System.out.print("Type the id of the pizza you crave: ");
	  String pizza = sc.next();
	 
	  // fetches all products not of category pizza from database and prints
	  commands.Print_Side_Dishes();
	  System.out.print("Type the id of the side dishes you want, seperated "
	      + "by a comma(no space): ");
	  String side_dishes = sc.next();
	  String[] all_products = (pizza + "," + side_dishes).split("[,]+");
	  
	  ArrayList<Integer> ids = new ArrayList<Integer>();
	  for (int i = 0; i < all_products.length;i++){
	    ids.add(Integer.parseInt(all_products[i]));
	  }
	  int input;
	  String standby = "";
	  System.out.print("Will this be a standing order(Y/N)");
	  if (sc.next().equals("Y")){
	    System.out.print("1. Weekly/n2.Biweekly/n3.Monthly/nInput:");
	    input = sc.nextInt();
	    
	    switch(input){
	      case 1:
	        standby = "weekly";
	        break;
	      case 2:
	        standby = "biweekly";
	        //updateStandby();
	        break;
	      case 3:
	        standby = "monthly";
	        //updateStandby();
	        break;
	      default:
	        break;
	    }
	  }
	  
	  double price = commands.calculate_price(ids);
	  Time time_delivered = commands.time_delivered(id);
	  
	  // delivery time returns the time when the delivery is made.
	  System.out.print("Your total comes out to " + price + "and the "
	      + "earliest possible time for delivery is " + time_delivered.toString() +
	      ". A delivery charge of "+ commands.get_delivery_charge(id) + "applies.");
	  
	  Date currentTime = new Date();
	  SimpleDateFormat ft = new SimpleDateFormat("yyyy.MM.dd");
	  Time accessTime = new Time(currentTime.getTime());
	  Time endTime = new Time(2*(time_delivered.getTime() - 
	      accessTime.getTime()) +accessTime.getTime());
	  //dispatches a driver
	  int driver_id = commands.dispatch(new Time(2*(time_delivered.getTime() - 
          accessTime.getTime())));
	  //record the transaction in the log book
	  commands.addOrder(id, ids, ft.format(currentTime), standby, driver_id, 
	      accessTime.toString(), time_delivered.toString(), endTime.toString());
	}
	
	

	private static void Suggested_Products(){ 
	  
		System.out.print("Customer ID:");
		int id = sc.nextInt();
	  
		//print the products
		commands.topPurchase(id);
	}

	/** get the orders for today for the specified customer
	 * and print them to the terminal **/
	private static void Todays_Orders() { 
	  
		System.out.print("Customer ID:");
		int id = sc.nextInt();
		
		System.out.println("Displaying all unfullfilled orders:");
		
		//get the orders
		ResultSet orders = commands.getCustomerCurrentOrders(id);
		
		if (orders == null) {
			System.out.println(error);
			return;
		}
		
		//print headers:
		String format = "%20d%20s%20.2f%20.2f%20s";
		String headerFormat = "%20s%20s%20s%20s%20s";
		System.out.format
			(headerFormat, "Transaction ID", "Delivery Date", "Price",
					"Delivery Charge", "Standby Status");
		System.out.println();
		
		try {
			
			while (orders.next()) {
				
				int tid = orders.getInt(1);
				String date = orders.getString(2);
				double price = orders.getDouble(3);
				double deliveryCharge = orders.getDouble(4);
				String standby = orders.getString(5);
				
				System.out.format(format, tid, date, price, 
						deliveryCharge, standby);
				System.out.println();
			}
		}
		
		catch (SQLException e) {
			System.err.println(e.toString());
			return;
		}
	}
	
	private static void All_Customer_Transactions() {
		
		System.out.print("Customer ID:");
		int id = sc.nextInt();
		  
		System.out.println("Displaying entire order history:");
		
		//get the orders
		ResultSet orders = commands.getCustomerAllOrders(id);
		
		if (orders == null) {
			System.out.println(error);
			return;
		}
		
		//print headers:
		String format = "%20d%20s%20s%20.2f%20.2f%20s";
		String headerFormat = "%20s%20s%20s%20s%20s%20s";
		System.out.format
			(headerFormat, "Transaction ID", "Status", "Delivery Date",
					"Price","Delivery Charge", "Standby Status");
		System.out.println();
		
		try {
			
			while (orders.next()) {
				
				int tid = orders.getInt(1);
				String status = orders.getString(2);
				String date = orders.getString(3);
				double price = orders.getDouble(4);
				double deliveryCharge = orders.getDouble(5);
				String standby = orders.getString(6);
				
				System.out.format(format, tid, status, date, price, 
						deliveryCharge, standby);
				System.out.println();
			}
		}
		
		catch (SQLException e) {
			System.err.println(e.toString());
			return;
		}
	}                                  

	/**
	 * Display all products to user 
	 */
	private static void displayProducts() {
		
		System.out.print("Displaying all products:");
		
		//get the products
		ResultSet products = commands.getAllProducts();
		
		if (products == null) {
			System.out.println(error);
			return;
		}
		
		//print headers:
		String format = "%20d%20s%20s%20.2f%20.2f";
		String headerFormat = "%20s%20s%20s%20s%20s";
		System.out.format(headerFormat, "Product ID", "Name", "Category",
				"Price","Sale Price");
		System.out.println();
		
		try {
			
			while (products.next()) {
				
				int pid = products.getInt(1);
				String name = products.getString(2);
				String cat = products.getString(3);
				double price = products.getDouble(4);
				double salePrice = products.getDouble(5);
				
				if (salePrice <= 0.0) salePrice = price;
					
				System.out.format(format, pid, name, cat, price, salePrice);
				System.out.println();
			}
		}
		
		catch (SQLException e) {
			System.err.println(e.toString());
			return;
		}
	}
} 
