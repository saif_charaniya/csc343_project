package data_managers;

import java.sql.*;
import java.util.ArrayList;
import java.util.Date;

import data_managers.Schema.ADDRESS;
import data_managers.Schema.CASH;
import data_managers.Schema.CHEQUE;
import data_managers.Schema.CREDIT_CARD;
import data_managers.Schema.CUSTOMER;
import data_managers.Schema.CUSTOMER_ADDRESS;
import data_managers.Schema.DELIVERIES;
import data_managers.Schema.DRIVER;
import data_managers.Schema.EMPLOYEE;
import data_managers.Schema.HAS_CAR;
import data_managers.Schema.HAS_PAYMENT;
import data_managers.Schema.ORDERS;
import data_managers.Schema.ORDER_ITEMS;
import data_managers.Schema.PAYMENT_METHOD;
import data_managers.Schema.PRODUCTS;
import data_managers.Schema.TRANSACTION;
import data_managers.Schema.VICHELE;

public class Commands {
  
	public static Connection connection;
	private static final String SPACE = SQLStatements.SPACE;
	private static final String COMMA = SQLStatements.COMMA;
	private static final String BR = SQLStatements.BR;
	private static final String BL = SQLStatements.BL;
	private static final String EQ = SQLStatements.EQ;
	private static final String QE = "\'";
	
	public Commands(Connection connection) {
		this.connection = connection;
	}

	/**
	 * Create a new row in customer table with specified values. Customer is
	 * 	assigned an ID and active status is defaulted to "active"
	 * @param fname -> first name 
	 * @param lname	-> last name
	 * @param gender -> gender: either M or F
	 * @param age -> age: in years
	 * @return ID of newly created customer, -1 if failure
	 */
	public int addCustomer (String fname, String lname, String gender,
			int age, double balance){
		
		String command = SQLStatements.INSERT + SPACE + SQLStatements.INTO + 
				SPACE + Schema.CUSTOMER_TABLE + SPACE + BL + CUSTOMER.FNAME +
				COMMA + CUSTOMER.LNAME + COMMA + CUSTOMER.GENDER + COMMA +
				CUSTOMER.AGE + COMMA + CUSTOMER.ACTIVE + COMMA +
				CUSTOMER.ACC_BALANCE + BR + SPACE + 
				SQLStatements.VALUES + SPACE + BL + QE + fname + QE + COMMA +
				QE + lname + QE + COMMA + QE + gender + QE + COMMA + age +
				COMMA + QE + "active" + QE + COMMA + balance + BR;
		
		String idCommand = SQLStatements.SELECT + SPACE + 
				SQLStatements.LAST_INSERT_ID + SPACE + SQLStatements.FROM +
				SPACE + Schema.CUSTOMER_TABLE;
		
		System.out.println(command);
		System.out.println(idCommand);

		int ID = -1;
		
		try {
			//add new customer
			
			Statement addNewCustomer = connection.createStatement();
			addNewCustomer.executeUpdate(command);
			
			// get the customer id
			ResultSet results = addNewCustomer.executeQuery(idCommand);
			if (results.next()){
				ID = Integer.parseInt(results.getString(1));
			}
		}
		
		catch (SQLException e) {
			System.err.println(e.toString());
		}
		
		return ID;
	}
	
	/**
	 * Update the customer whose has ID with the non null parameters given.
	 * Cannot deactivate or reactivate customer with this.
	 * @param ID
	 * @param fname
	 * @param lname
	 * @param gender
	 * @param age
	 * @return 1 on success, -1 on failure
	 * @throws SQLException 
	 */
	public int updateCustomer (int ID, String fname, String lname, 
			String gender, int age) {
		
		
		if (fname.length() > 0) {
			int result = updateCustomerHelper
					(ID, CUSTOMER.FNAME, QE + fname + QE);
			if (result == -1) return -1;
		}
		
		if (lname.length() > 0) {
			int result = updateCustomerHelper
					(ID, CUSTOMER.LNAME, QE + lname + QE);
			if (result == -1) return -1;
		}
		
		if (gender.length() > 0) {
			int result = updateCustomerHelper
					(ID, CUSTOMER.GENDER, QE + gender + QE);
			if (result == -1) return -1;
		}
		
		if (age > 0) {
			int result = updateCustomerHelper
					(ID, CUSTOMER.AGE, Integer.toString(age));
			if (result == -1) return -1;
		}
		
		return 1;
	}
	
	/**
	 * update specified customer's attribute with the given value
	 * @param ID
	 * @param item
	 * @param value
	 * @return -1 on failure, 1 on success
	 */
	private int updateCustomerHelper (int ID, String item, String value) {
				
		String command = SQLStatements.UPDATE + SPACE + Schema.CUSTOMER_TABLE +
				SPACE + SQLStatements.SET + SPACE + item + EQ +
				value + SPACE + SQLStatements.WHERE + SPACE +
				Schema.CUSTOMER.ID + EQ + ID;
		
		try {
			Statement deactivateCustomer = connection.createStatement();
			deactivateCustomer.executeUpdate(command);
			return 1;
		}
		
		catch (SQLException e) {
			System.err.println(e.toString());
			return -1;
		}		
	}
	
	/**
	 * helper function to add has payment between a payment and a customer
	 * @param customerID
	 * @param paymentID
	 * @return 1 on success, -1 on failure
	 */
	private int addHasPayment(int customerID, int paymentID) {
		
		//add to has_payment table
		String command = SQLStatements.INSERT + SPACE + SQLStatements.INTO 
				+ SPACE + Schema.HAS_PAYMENT_TABLE + SPACE + BL +
				HAS_PAYMENT.CUSTOMER + COMMA + HAS_PAYMENT.PAYMENT_METHOD +
				BR + SPACE + SQLStatements.VALUES + SPACE + BL +
				customerID + COMMA + paymentID + BR;
		
		try {
			
			System.out.println(command);
			Statement addHasPayment = connection.createStatement();
			addHasPayment.executeUpdate(command);
			
			//on success
			return 1;
		}
		
		catch (SQLException e) {
			System.err.println(e.toString());
			return -1;
		}
	}
	
	/** 
	 * helper function to add new payment method in payment_method table
	 * @param type
	 * @return paymentID on success, -1 on failure;
	 */
	private int addNewPayment(String type) {
		
		type = QE + type + QE;
		
		String command1 = SQLStatements.INSERT + SPACE + SQLStatements.INTO + 
				SPACE + Schema.PAYMENT_METHOD_TABLE + SPACE + BL + 
				PAYMENT_METHOD.TYPE + BR + SPACE + SQLStatements.VALUES + SPACE 
				+ BL + type + BR;
		
		String idCommand = SQLStatements.SELECT + SPACE + 
				SQLStatements.LAST_INSERT_ID + SPACE + SQLStatements.FROM +
				SPACE + Schema.PAYMENT_METHOD_TABLE;
		
		try {
			
			//add to payment method table
			System.out.println(command1);
			Statement addPayment = connection.createStatement();
			addPayment.executeUpdate(command1);
			
			
			int paymentID = 0;
			// get the payment_id
			ResultSet results = addPayment.executeQuery(idCommand);
			if (results.next()){
				paymentID = results.getInt(1);
			}
			
			return paymentID;
		}
		
		catch (SQLException e) {
			System.err.println(e.toString());
			return -1;
		}
	}
	
	/**
	 * Add a credit payment method for a new customer. This function will create 
	 * a new payment_method and connect the customer via has_payment table. To
	 * used when adding new customers
	 * @param customerId
	 * @param exp
	 * @param ccv
	 * @param ccnumber
	 * @return 1 on success , -1 on failure
	 */
	public int addCreditPayment(int customerID, String exp, String ccv,
			String ccnumber) {
		
		exp = QE + exp + QE;
		ccv = QE + ccv + QE;
		ccnumber = QE + ccnumber + QE;
		
		try {
			
			int paymentID = addNewPayment("CREDIT");
			if (paymentID == -1) return -1;
			
			//add to cheque table
			String command = SQLStatements.INSERT + SPACE + SQLStatements.INTO 
					+ SPACE + Schema.CREDIT_CARD_TABLE + SPACE + BL + 
					CREDIT_CARD.PAYMENT_METHOD_ID + COMMA +
					CREDIT_CARD.CARD_NUMBER + COMMA + CREDIT_CARD.CARD_CCV + 
					COMMA + CREDIT_CARD.CARD_EXP_DATE + BR + SPACE + 
					SQLStatements.VALUES + SPACE + BL + paymentID + COMMA + 
					ccnumber + COMMA + ccv + COMMA + exp + BR;
			
			System.out.println(command);
			Statement addCredit = connection.createStatement();
			addCredit.executeUpdate(command);
			
			//add to has payment
			if (addHasPayment(customerID, paymentID) == -1) return -1;
			
			//on success
			return 1;
		}
		
		catch (SQLException e) {
			System.err.println(e.toString());
			return -1;
		}
	}
	
	/**
	 * Add a cheque payment method for a new customer. This function will create 
	 * a new payment_method and connect the customer via has_payment table. To
	 * used when adding new customers
	 * @param customerID
	 * @param acc_number
	 * @param balance
	 * @return 1 on success, -1 on failure;
	 */
	public int addChequePayment (int customerID, String acc_number, 
			double balance){
		
		acc_number = QE + acc_number + QE;
		
		try {
			
			int paymentID = addNewPayment("CHEQUE");
			if (paymentID == -1) return -1;
			
			//add to cheque table
			String command2 = SQLStatements.INSERT + SPACE + SQLStatements.INTO 
					+ SPACE + Schema.CHEQUE_TABLE + SPACE + BL + 
					CHEQUE.PAYMENT_METHOD_ID + COMMA + CHEQUE.ACCOUNT_NUMBER + 
					COMMA + CHEQUE.BALANCE_LIMIT + BR + SPACE + 
					SQLStatements.VALUES + SPACE + BL + paymentID + COMMA +
					acc_number + COMMA + balance + BR;
			
			System.out.println(command2);
			Statement addCheque = connection.createStatement();
			addCheque.executeUpdate(command2);
			
			//add to has payment
			if (addHasPayment(customerID, paymentID) == -1) return -1;
			
			//on success
			return 1;
		}
		
		catch (SQLException e) {
			System.err.println(e.toString());
			return -1;
		}
		
	}
	/**
	 * Add a cash payment method to a customer.  This function will create 
	 * a new payment_method and connect the customer via has_payment table. To
	 * used when adding new customers
	 * @param customerID
	 * @return 1 on success, -1 on failure
	 */
	public int addCashPayment(int customerID) {
		
		try {
			
			int paymentID = addNewPayment("CASH");
			if (paymentID == -1) return -1;
			
			//add to cash table
			String command2 = SQLStatements.INSERT + SPACE + SQLStatements.INTO 
					+ SPACE + Schema.CASH_TABLE + SPACE + BL + 
					CASH.PAYMENT_METHOD_ID + BR + SPACE + SQLStatements.VALUES 
					+ SPACE + BL + paymentID + BR;
			
			System.out.println(command2);
			Statement addCash = connection.createStatement();
			addCash.executeUpdate(command2);
			
			//add to has payment
			if (addHasPayment(customerID, paymentID) == -1) return -1;
			
			//finished
			return 1;
		}
		
		catch (SQLException e) {
			System.err.println(e.toString());
			return -1;
		}
		
	}
	
	/**
	 * Sets Active attribute in Customer table to "deactive" for Customer with
	 * ID = customerID
	 * @param customerID
	 * @return 1 on success, -1 on failure
	 */
	public int deactivateCustomer (int customerID) {
		
		return updateCustomerHelper
				(customerID, CUSTOMER.ACTIVE, "\'deactive\'");
	}
	
	/**
	 * Sets Active attribute in Customer table to "active" for Customer with
	 * ID = customerID
	 * @param customerID
	 * @return 1 on success, -1 on failure
	 */
	public int reactivateCustomer (int customerID) {
		
		return updateCustomerHelper(customerID, CUSTOMER.ACTIVE, "\'active\'");
	}
	
	/**
	 * Create new Address for customer with ID customerID and link in the 
	 * customer address table
	 * @param customerID
	 * @param country
	 * @param state
	 * @param city
	 * @param street
	 * @param street_number
	 * @param unit_number
	 * @param postal_code
	 * @return 1 on success, -1 on failure
	 */
	public int createAddress(int customerID, String country, String state, 
			String city, String street, int street_number, int unit_number, 
			String postal_code, double dilivery_charge) {
		
		country = QE + country + QE;
		postal_code = QE + postal_code + QE;
		state = QE + state + QE;
		city = QE + city + QE;
		street = QE + street + QE;
		
		String command1 = SQLStatements.INSERT + SPACE + SQLStatements.INTO + 
				SPACE + Schema.ADDRESS_TABLE + SPACE + BL + ADDRESS.COUNTRY + 
				COMMA + ADDRESS.STATE + COMMA + ADDRESS.CITY + COMMA + 
				ADDRESS.STREET + COMMA + ADDRESS.STREET_NUMBER + COMMA + 
				ADDRESS.UNIT_NUMBER + COMMA + ADDRESS.POSTAL_CODE + COMMA +
				ADDRESS.DELIVERY_CHARGE + BR + SPACE +
				SQLStatements.VALUES + SPACE + BL + country + COMMA + state + 
				COMMA + city + COMMA + street + COMMA + street_number + COMMA +
				unit_number + COMMA + postal_code + COMMA + dilivery_charge + 
				BR;
		
		//add both address and customer to Customer address table
		String command2 = SQLStatements.INSERT + SPACE + SQLStatements.INTO
				+ SPACE + Schema.CUSTOMER_ADDRESS_TABLE + SPACE + BL + 
				CUSTOMER_ADDRESS.POSTAL_CODE + COMMA + 
				CUSTOMER_ADDRESS.STREET + COMMA + 
				CUSTOMER_ADDRESS.STREET_NUMBER + COMMA +
				CUSTOMER_ADDRESS.UNIT_NUMBER + COMMA +
				CUSTOMER_ADDRESS.CUSTOMER + BR + SPACE + SQLStatements.VALUES + 
				SPACE + BL + postal_code + COMMA + street + COMMA + 
				street_number + COMMA + unit_number + COMMA + customerID + BR;
		
		System.out.println(command1);
		System.out.println(command2);
		
		try {
			//add new customer
			
			Statement addNewAddress = connection.createStatement();
			addNewAddress.executeUpdate(command1);
			
			Statement addNewCustomerAddress = connection.createStatement();
			addNewCustomerAddress.executeUpdate(command2);
			
			return 1;
		}
		
		catch (SQLException e) {
			System.err.println(e.toString());
			return -1;
		}
	}
	
	
	
	/**
	 * Create a driver which is an employee.
	 * @param fname
	 * @param lname
	 * @param salary
	 * @return the driver employee id, -1 on failure
	 */
	public int createDriver(String fname, String lname, double salary) {
		
		lname = QE + lname + QE;
		fname = QE + fname + QE;
		
		String command = SQLStatements.INSERT + SPACE + SQLStatements.INTO + 
				SPACE + Schema.EMPLOYEE_TABLE + SPACE + BL + EMPLOYEE.FNAME + 
				COMMA + EMPLOYEE.LNAME + COMMA + EMPLOYEE.SALARY + BR + SPACE +
				SQLStatements.VALUES + SPACE + BL + fname + COMMA + lname + 
				COMMA + salary + BR;
		System.out.println(command);
		
		String idCommand = SQLStatements.SELECT + SPACE + 
				SQLStatements.LAST_INSERT_ID + SPACE + SQLStatements.FROM +
				SPACE + Schema.EMPLOYEE_TABLE;
		
		int ID = -1;
		
		try {
			//add new employee
			
			Statement addNewEmployee = connection.createStatement();
			addNewEmployee.executeUpdate(command);
			
			// get the customer id
			ResultSet results = addNewEmployee.executeQuery(idCommand);
			if (results.next()){
				ID = Integer.parseInt(results.getString(1));
			}
			
			//add employee as driver
			String command2 = SQLStatements.INSERT + SPACE + SQLStatements.INTO
					+ SPACE + Schema.DRIVER_TABLE + SPACE + BL 
					+ DRIVER.EMPLOYEE_ID + BR + SPACE + SQLStatements.VALUES +
					SPACE + BL + ID + BR;
			System.out.println(command2);
			Statement addNewDriver = connection.createStatement();
			addNewDriver.executeUpdate(command2);
		}
		
		catch (SQLException e) {
			System.err.println(e.toString());
			return -1;
		}
		
		return ID;
		
	}
	
	/**
	 * Assign to the driver the car outlined in the parameters
	 * @param driverID
	 * @param make
	 * @param model
	 * @param color
	 * @param license
	 * @param cpm
	 * @return 1 on success , -1 on failure
	 */
	public int createCar(int driverID, String make, String model, String color,
			String license, double cpm) {
		
		make = QE + make + QE;
		model = QE + model + QE;
		color = QE + color + QE;
		license = QE + license + QE;
		
		String command = SQLStatements.INSERT + SPACE + SQLStatements.INTO + 
				SPACE + Schema.VICHELE_TABLE + SPACE + BL + VICHELE.MAKE + COMMA
				+ VICHELE.MODEL + COMMA + VICHELE.COLOUR + COMMA +
				VICHELE.LICENSE + COMMA + VICHELE.CPM + BR + SPACE +
				SQLStatements.VALUES + SPACE + BL + make + COMMA + model + COMMA
				+ color + COMMA + license + COMMA + cpm + BR;
		
		System.out.println(command);
		
		String command2 = SQLStatements.INSERT + SPACE + SQLStatements.INTO + 
				SPACE + Schema.HAS_CAR_TABLE + SPACE + BL + HAS_CAR.DRIVER + 
				COMMA + HAS_CAR.VICHELE + BR + SPACE + SQLStatements.VALUES + 
				SPACE + BL + driverID + COMMA + license + BR;
		
		System.out.println(command2);
		
		try {
			//add new employee
			
			Statement addNewCar = connection.createStatement();
			addNewCar.executeUpdate(command);
			
			Statement addNewHasCar = connection.createStatement();
			addNewHasCar.executeUpdate(command2);
			
			return 1;
		}
		
		catch (SQLException e) {
			System.err.println(e.toString());
			return -1;
		}
	}
	
	/**
	 * Return account balance for the customer
	 * @param customerID -> of the customer 
	 * @return account balance, null on failure
	 */
	public double getCustomerBalance(int customerID) {
		
		String command = SQLStatements.SELECT + SPACE + CUSTOMER.ACC_BALANCE 
				+ SPACE + SQLStatements.FROM + SPACE + Schema.CUSTOMER_TABLE + 
				SPACE + SQLStatements.WHERE + SPACE + CUSTOMER.ID + EQ + 
				customerID;
		
		Double balance = -1.0;
		try {
			
			System.out.println(command);
			Statement getBalance = connection.createStatement();
			
			// get the customer id
			ResultSet results = getBalance.executeQuery(command);
			
			if (results.next()){
				balance = Double.parseDouble(results.getString(1));
			}
		}
		
		catch (SQLException e) {
			System.err.println(e.toString());
		}
		
		return balance;
	}
	
	

	public double calculate_price(ArrayList<Integer> productID){
	
	//find price 
	  double price = 0;
      try {
          
          Statement getPrice = connection.createStatement();

          for (int i = 0; i < productID.size(); i++) {
              
              int pid = productID.get(i);
              String command = SQLStatements.SELECT + SPACE + PRODUCTS.PRICE +
                      COMMA + PRODUCTS.TAXABLE + SPACE + SQLStatements.FROM 
                      + SPACE + Schema.PRODUCTS_TABLE + SPACE + 
                      SQLStatements.WHERE + SPACE + PRODUCTS.ID + EQ + pid;
              
              System.out.println(command);
              // get the customer id
              ResultSet results = getPrice.executeQuery(command);
              if (results.next()){
            	  //price = price * tax
                  price += results.getDouble(1) * results.getDouble(2);
              }
          }
      }
      catch (SQLException e) {
          System.err.println(e.toString());
          return -1;
      }
      return price;
	}
	
	public Time time_delivered(int customerID){
	  
	  Date currentTime = new Date();
	  double money = get_delivery_charge(customerID);
	  int driveTime = 0;
	  if (money>5){
	    driveTime = 10;
	  }
	  else{
	    driveTime = 5;
	  }
	  
	  Time delivery_time = new Time(currentTime.getTime() + driveTime * 60000);
	  return delivery_time;
	}
	
	public double get_delivery_charge(int customerID){

		//find delivery charge from customer address
      try {
          
          String command = SQLStatements.SELECT + SPACE + 
                  ADDRESS.DELIVERY_CHARGE + SPACE + SQLStatements.FROM + SPACE
                   + Schema.CUSTOMER_ADDRESS_TABLE + COMMA + Schema.ADDRESS_TABLE +
                  SPACE + SQLStatements.WHERE + SPACE + Schema.ADDRESS_TABLE + "." +
                  ADDRESS.POSTAL_CODE + EQ + Schema.CUSTOMER_ADDRESS_TABLE + "." +
                  CUSTOMER_ADDRESS.POSTAL_CODE + SPACE + SQLStatements.AND + 
                  SPACE + Schema.ADDRESS_TABLE + "." +
                  ADDRESS.STREET + EQ + Schema.CUSTOMER_ADDRESS_TABLE + "." +
                  CUSTOMER_ADDRESS.STREET + SPACE + SQLStatements.AND + 
                  SPACE + Schema.ADDRESS_TABLE + "." +
                  ADDRESS.STREET_NUMBER + EQ + Schema.CUSTOMER_ADDRESS_TABLE + "." +
                  CUSTOMER_ADDRESS.STREET_NUMBER + SPACE + SQLStatements.AND + 
                  SPACE + Schema.ADDRESS_TABLE + "." +
                  ADDRESS.UNIT_NUMBER + EQ + Schema.CUSTOMER_ADDRESS_TABLE + "." +
                  CUSTOMER_ADDRESS.UNIT_NUMBER + SPACE + SQLStatements.AND + 
                  SPACE + Schema.CUSTOMER_ADDRESS_TABLE + "." +
                  CUSTOMER_ADDRESS.CUSTOMER + EQ + customerID;
          
          System.out.println(command);
          Statement getDeliveryCharge = connection.createStatement();

          // get the customer delivery charge
          ResultSet results = getDeliveryCharge.executeQuery(command);
          if (results.next()){
          }
          
          return results.getDouble(1);
      }
      catch (SQLException e) {
          System.err.println(e.toString());
          return -1.;
      }
	}
	
	/**
	 * Add a transaction to the system by doing the following
	 * Determine price by summing price of all products, Determine delivery 
	 * charge based on customer address, add products to has_order table,
	 * add customer id and transaction id to order table
	 * @param customerID
	 * @param productID -> arraylist of product ids order
	 * @param date -> delivery date in sql format YYYY-MM-DD
	 * @param standby
	 * @return transaction id on success, -1 on failure
	 */
	public int addOrder(int customerID, ArrayList<Integer> productID,
			String date, String standby) {
		
		double price = calculate_price(productID);
		double delivery_charge = get_delivery_charge(customerID);
		standby = QE + standby + QE;
		String status = QE + "not delivered" + QE;
		date = QE + date + QE;
		int transactionID = -1;
		
		//insert the new transaction item
		try {
			
			String command = SQLStatements.INSERT + SPACE + SQLStatements.INTO +
					SPACE + Schema.TRANSACTION_TABLE + SPACE + BL + 
					TRANSACTION.PRICE + COMMA + TRANSACTION.DILIVERY_CHARGE + 
					COMMA + TRANSACTION.DATE + COMMA + TRANSACTION.STANDBY + 
					COMMA + TRANSACTION.STATUS + BR + SPACE + 
					SQLStatements.VALUES + SPACE + BL + price + COMMA + 
					delivery_charge + COMMA + date + COMMA + standby + COMMA +
					status + BR;
			
			String idCommand = SQLStatements.SELECT + SPACE + 
					SQLStatements.LAST_INSERT_ID + SPACE + SQLStatements.FROM +
					SPACE + Schema.CUSTOMER_TABLE;
			
			System.out.println(command);
			Statement addTrans = connection.createStatement();
			addTrans.executeUpdate(command);
			
			// get the transaction id
			System.out.println(idCommand);
			ResultSet results = addTrans.executeQuery(idCommand);
			if (results.next()){
				transactionID = results.getInt(1);
			}
			
		}
		catch (SQLException e){
			System.out.println(e.toString());
			return -1;
		}
		
		//insert in order the customerID and transaction id
		try {
			
			String command = SQLStatements.INSERT + SPACE + SQLStatements.INTO +
					SPACE + Schema.ORDERS_TABLE + SPACE + BL + 
					ORDERS.CUSTOMER + COMMA + ORDERS.TRANSACTION + BR + SPACE + 
					SQLStatements.VALUES + SPACE + BL + customerID + COMMA +
					transactionID + BR;
			
			System.out.println(command);
			Statement addOrder = connection.createStatement();
			addOrder.executeUpdate(command);
		}
		catch (SQLException e){
			System.out.println(e.toString());
			return -1;
		}
		
		//add all products orders and transaction id into order items table
		try {
			
			for (int i = 0; i < productID.size(); i++) {
				
				String command = SQLStatements.INSERT + SPACE + 
						SQLStatements.INTO +
					SPACE + Schema.ORDER_ITEMS_TABLE + SPACE + BL + 
					ORDER_ITEMS.PRODUCT_ID + COMMA + ORDER_ITEMS.TRANSACTION_ID
					+ BR + SPACE + SQLStatements.VALUES + SPACE + BL + 
					productID.get(i) + COMMA + transactionID + BR;
			
				System.out.println(command);
				Statement addOrderItem = connection.createStatement();
				addOrderItem.executeUpdate(command);
			}
		}
		catch (SQLException e){
			System.out.println(e.toString());
			return -1;
		}
		
		//update the customers acc balance by subtracting the price and delivery
		double total_cost = price + delivery_charge;
		try {
			
			String command = SQLStatements.UPDATE + SPACE + 
					Schema.CUSTOMER_TABLE + SPACE + SQLStatements.SET + SPACE +
					CUSTOMER.ACC_BALANCE + EQ + CUSTOMER.ACC_BALANCE + SPACE + 
					"-" + total_cost + SPACE + SQLStatements.WHERE + SPACE +
					CUSTOMER.ID + EQ + customerID;
			
			System.out.println(command);
			Statement addOrder = connection.createStatement();
			addOrder.executeUpdate(command);
		}
		catch (SQLException e){
			System.out.println(e.toString());
			return -1;
		}
		
		//transaction complete
		return transactionID;
		
	}
	
	public int addOrder(int customerID, ArrayList<Integer> productID,
			String date, String standby, int driver_id, String start, String mid,
			String end) {
		
		double price = calculate_price(productID);
		double delivery_charge = get_delivery_charge(customerID);
		standby = QE + standby + QE;
		String status = QE + "not delivered" + QE;
		date = QE + date + QE;
		int transactionID = -1;
		
		start = QE + start + QE;
		mid = QE + mid + QE;
		end = QE + end + QE;
		
		
		
		System.out.println("Diliver charge: " + delivery_charge);
        System.out.println("Price: " + price);
		
		//insert the new transaction item
		try {
			
			String command = SQLStatements.INSERT + SPACE + SQLStatements.INTO +
					SPACE + Schema.TRANSACTION_TABLE + SPACE + BL + 
					TRANSACTION.PRICE + COMMA + TRANSACTION.DILIVERY_CHARGE + 
					COMMA + TRANSACTION.DATE + COMMA + TRANSACTION.STANDBY + 
					COMMA + TRANSACTION.STATUS+ COMMA +
					TRANSACTION.START_TIME + COMMA +
					TRANSACTION.ARRIVED_CUSTOMER_TIME + COMMA +
					TRANSACTION.COME_HOME_TIME + BR + SPACE + 
					SQLStatements.VALUES + SPACE + BL + price + COMMA + 
					delivery_charge + COMMA + date + COMMA + standby + COMMA +
					status + COMMA + start + COMMA + mid + COMMA + end + BR;
			
			String idCommand = SQLStatements.SELECT + SPACE + 
					SQLStatements.LAST_INSERT_ID + SPACE + SQLStatements.FROM +
					SPACE + Schema.CUSTOMER_TABLE;
			
			System.out.println(command);
			Statement addTrans = connection.createStatement();
			addTrans.executeUpdate(command);
			
			// get the transaction id
			System.out.println(idCommand);
			ResultSet results = addTrans.executeQuery(idCommand);
			if (results.next()){
				transactionID = results.getInt(1);
			}
			
		}
		catch (SQLException e){
			System.out.println(e.toString());
			return -1;
		}
		
		//insert in order the customerID and transaction id
		try {
			
			String command = SQLStatements.INSERT + SPACE + SQLStatements.INTO +
					SPACE + Schema.ORDERS_TABLE + SPACE + BL + 
					ORDERS.CUSTOMER + COMMA + ORDERS.TRANSACTION + BR + SPACE + 
					SQLStatements.VALUES + SPACE + BL + customerID + COMMA +
					transactionID + BR;
			
			System.out.println(command);
			Statement addOrder = connection.createStatement();
			addOrder.executeUpdate(command);
		}
		catch (SQLException e){
			System.out.println(e.toString());
			return -1;
		}
		
		//add all products orders and transaction id into order items table
		try {
			
			for (int i = 0; i < productID.size(); i++) {
				
				String command = SQLStatements.INSERT + SPACE + 
						SQLStatements.INTO +
					SPACE + Schema.ORDER_ITEMS_TABLE + SPACE + BL + 
					ORDER_ITEMS.PRODUCT_ID + COMMA + ORDER_ITEMS.TRANSACTION_ID
					+ BR + SPACE + SQLStatements.VALUES + SPACE + BL + 
					productID.get(i) + COMMA + transactionID + BR;
			
				System.out.println(command);
				Statement addOrderItem = connection.createStatement();
				addOrderItem.executeUpdate(command);
			}
		}
		catch (SQLException e){
			System.out.println(e.toString());
			return -1;
		}
		
		//update the customers acc balance by subtracting the price and delivery
		double total_cost = price + delivery_charge;
		try {
			
			String command = SQLStatements.UPDATE + SPACE + 
					Schema.CUSTOMER_TABLE + SPACE + SQLStatements.SET + SPACE +
					CUSTOMER.ACC_BALANCE + EQ + CUSTOMER.ACC_BALANCE + SPACE + 
					"-" + total_cost + SPACE + SQLStatements.WHERE + SPACE +
					CUSTOMER.ID + EQ + customerID;
			
			System.out.println(command);
			Statement addOrder = connection.createStatement();
			addOrder.executeUpdate(command);
		}
		catch (SQLException e){
			System.out.println(e.toString());
			return -1;
		}
		
		//UPDATE DELIVERIES TABLE WITH DRIVER AND TRANSACTION ID
		try {
			
			String command = SQLStatements.INSERT + SPACE + SQLStatements.INTO +
					SPACE + Schema.DELIVERIES_TABLE + SPACE + BL + 
					DELIVERIES.DRIVER + COMMA + DELIVERIES.TRANSACTION + BR + 
					SPACE + SQLStatements.VALUES + SPACE + BL + driver_id + 
					COMMA + transactionID + BR;
			
			System.out.println(command);
			Statement addDeliveries = connection.createStatement();
			addDeliveries.executeUpdate(command);
		}
		catch (SQLException e){
			System.out.println(e.toString());
			return -1;
		}
		
		//transaction complete
		return transactionID;
		
	}
	
	public void Print_Pizza_Menu(){
		try{
	  Statement statement = connection.createStatement();
	  ResultSet rs = null;
	try {
		rs = statement.executeQuery("SELECT id, price, product_name"
		      + " FROM products WHERE category=\'pizza\'");
	} catch (Exception e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
	  ResultSetMetaData rsMetaData = null;
	try {
		rsMetaData = rs.getMetaData();
	} catch (Exception e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
	  int numberOfColumns = 0;
	try {
		numberOfColumns = rsMetaData.getColumnCount();
	} catch (Exception e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
	  
	  
	  System.out.println();
	  
	String format = "%20d%20s%20.2f";
	String headerFormat = "%20s%20s%20s";
	System.out.format
		(headerFormat, "Product ID", "Name", "Price");
	System.out.println();
	  try {
		while (rs.next())
		  {
		      int pid = rs.getInt(1);
		      String name = rs.getString(3);
		      double price = rs.getDouble(2);
		      System.out.format(format, pid, name, price);
		      System.out.println();
		  }
	} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
		}
		catch (Exception e) {}
	}
	
	public void Print_Side_Dishes() {
		
	  Statement statement = null;
	try {
		statement = connection.createStatement();
	} catch (SQLException e2) {
		// TODO Auto-generated catch block
		e2.printStackTrace();
	}
	  ResultSet rs = null;
	try {
		rs = statement.executeQuery("SELECT id,price,product_name"
		      + " FROM products WHERE category <> \'pizza\'");
	} catch (SQLException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
	  ResultSetMetaData rsMetaData = null;
	try {
		rsMetaData = rs.getMetaData();
	} catch (SQLException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
	  int numberOfColumns = 0;
	try {
		numberOfColumns = rsMetaData.getColumnCount();
	} catch (SQLException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
	  
	  String format = "%20d%20s%20.2f";
	  String headerFormat = "%20s%20s%20s";
		System.out.format
			(headerFormat, "Product ID", "Name", "Price");
		System.out.println();
		  try {
			while (rs.next())
			  {
			      int pid = rs.getInt(1);
			      String name = rs.getString(3);
			      double price = rs.getDouble(2);
			      System.out.format(format, pid, name, price);
			      System.out.println();
			  }
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	}
	
	// returns id of the driver whose available the earliest
	public int Find_Least_Busy_Driver(){
	  Statement statement = null;
	try {
		statement = connection.createStatement();
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	  int id = 0;
      ResultSet rs = null;
	try {
		rs = statement.executeQuery("SELECT " + DRIVER.EMPLOYEE_ID + ",MIN("+ DRIVER.FREE_WHEN+") FROM driver");
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
      try {
		if(rs.next()){
		    id = rs.getInt(1);
		  }
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
      return id;
	}
	
	public int dispatch(Time deliverTimeDiff) {
	  
		int id = Find_Least_Busy_Driver();
		Statement statement = null;
		try {
			statement = connection.createStatement();
		} 
		catch (SQLException e) {
			e.printStackTrace();
		}
		long free_when = 0;
		ResultSet rs = null;
		try {
			rs = statement.executeQuery("SELECT "+ DRIVER.FREE_WHEN +" FROM driver"
		       + " WHERE " + DRIVER.EMPLOYEE_ID + " = " + id);
		} catch (SQLException e) {
		
			e.printStackTrace();
		}
		
		try {
			
			if(rs.next()) {
				try {
					free_when = rs.getLong(1);
				} 
				catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				}
			}		
		} 
		catch (SQLException e) {
		// TODO Auto-generated catch block
			e.printStackTrace();
		}
	   
		try {
			statement.executeQuery("UPDATE drivers SET free_when = " 
		   + (free_when + deliverTimeDiff.getTime())
		       + " WHERE id = " + id);
		} 
		catch (SQLException e) {
		// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return id;
	}
	
	/**
	 * Get all unfullfilled transactions for the specified customer
	 * @return ResultSet / cursor of the rows with the following attributes:
	 * Transaction/Ticket ID, Delivery Date, Price, Delivery Charge, Standby 
	 * Value.
	 * @param customerID -> customer to retrieve transactions for 
	 */
	public ResultSet getCustomerCurrentOrders (int customerID) {
		
		String command = SQLStatements.SELECT + SPACE + 
				Schema.TRANSACTION_TABLE + "." + TRANSACTION.ID + COMMA +
				TRANSACTION.DATE + COMMA + TRANSACTION.PRICE + COMMA + 
				TRANSACTION.DILIVERY_CHARGE + COMMA + TRANSACTION.STANDBY + 
				SPACE + SQLStatements.FROM + SPACE + 
				Schema.TRANSACTION_TABLE + COMMA +
				Schema.ORDERS_TABLE + SPACE + SQLStatements.WHERE + SPACE + 
				Schema.ORDERS_TABLE + "." + ORDERS.CUSTOMER + EQ + customerID + 
				SPACE + SQLStatements.AND + SPACE +
				Schema.TRANSACTION_TABLE + "." + TRANSACTION.ID + EQ + 
				Schema.ORDERS_TABLE + "." + ORDERS.TRANSACTION + SPACE + 
				SQLStatements.AND + SPACE + TRANSACTION.STATUS + SPACE +
				SQLStatements.NOT + SPACE + SQLStatements.LIKE + SPACE + QE + 
				"delivered" + QE;

		try {
			
			System.out.println(command);
			Statement getOrder = connection.createStatement();
			ResultSet results = getOrder.executeQuery(command);
			return results;
		}
		
		catch (SQLException e) {
			System.out.println(e.toString());
			return null;
		}
		
	}
	
	/**
	 * Get all transactions for the specified customer's entire history
	 * @return ResultSet / cursor of the rows with the following attributes:
	 * Transaction/Ticket ID, Status, Delivery Date, Price, Delivery Charge, 
	 * Standby Value.
	 * @param customerID -> customer to retrieve transactions for 
	 */
	public ResultSet getCustomerAllOrders (int customerID) {
		
		String command = SQLStatements.SELECT + SPACE + 
				Schema.TRANSACTION_TABLE + "." + TRANSACTION.ID + COMMA +
				TRANSACTION.STATUS + COMMA + TRANSACTION.DATE + COMMA + 
				TRANSACTION.PRICE + COMMA + TRANSACTION.DILIVERY_CHARGE + COMMA 
				+ TRANSACTION.STANDBY + SPACE + SQLStatements.FROM + SPACE + 
				Schema.TRANSACTION_TABLE + COMMA + Schema.ORDERS_TABLE + SPACE +
				SQLStatements.WHERE + SPACE + Schema.ORDERS_TABLE + "." + 
				ORDERS.CUSTOMER + EQ + customerID + SPACE + SQLStatements.AND + 
				SPACE + Schema.TRANSACTION_TABLE + "." + TRANSACTION.ID + EQ + 
				Schema.ORDERS_TABLE + "." + ORDERS.TRANSACTION;

		try {
			
			System.out.println(command);
			Statement getOrder = connection.createStatement();
			ResultSet results = getOrder.executeQuery(command);
			return results;
		}
		
		catch (SQLException e) {
			System.out.println(e.toString());
			return null;
		}
		
	}
	
	public int standbyOrder () {
		
		return 0;
	}
	
	public int listAllCustomersByItem () {
		
		return 0;
	}
	
	public int allDriversWorking() {
		
		return 0;
	}
	
	public int weeklySales () {
		
		return 0;
	}	
	
	/** 
	 * retrieve all rows from the products table
	 * @return a ResultSet/Cursor with the following attributes: product id, 
	 * name, category, price, sale price
	 */
	public ResultSet getAllProducts () {
		
		String command = SQLStatements.SELECT + SPACE + PRODUCTS.ID + COMMA +
				PRODUCTS.NAME + COMMA + PRODUCTS.CATEGORY + COMMA +
				PRODUCTS.PRICE + COMMA + PRODUCTS.SALE_PRICE + SPACE + 
				SQLStatements.FROM + SPACE + Schema.PRODUCTS_TABLE;
		try {
			
			System.out.println(command);
			Statement getProducts = connection.createStatement();
			ResultSet results = getProducts.executeQuery(command);
			return results;
		}
		
		catch (SQLException e) {
			System.out.println(e.toString());
			return null;
		}
		
	}
	
	/**
	 * Find all the customers that we consider delinquent, defined as having a 
	 * balance < 0 and or who have not order an items in over 10 days.
	 * @param todayDate -> today's date in format "YYYY-MM-DD"
	 * @return ResultSet / cursor with the following attributes: Customer Id, 
	 * first name, last name, account balance.
	 */
	public ResultSet delinquentCustomers(String todayDate) {
			
		String command = SQLStatements.SELECT + SPACE + SQLStatements.DISTINCT + 
				SPACE + Schema.CUSTOMER_TABLE + "." + CUSTOMER.ID + COMMA +
				CUSTOMER.FNAME + COMMA + CUSTOMER.LNAME + COMMA + 
				CUSTOMER.ACC_BALANCE + SPACE + SQLStatements.FROM + SPACE + 
				Schema.CUSTOMER_TABLE + COMMA + 
				Schema.TRANSACTION_TABLE + SPACE + SQLStatements.WHERE + SPACE +
				CUSTOMER.ACC_BALANCE + SQLStatements.LESS + "0.0" + SPACE +
				SQLStatements.OR + SPACE + BL + 
				Schema.TRANSACTION_TABLE + "." + TRANSACTION.ID + EQ + SPACE +
				BL + SQLStatements.SELECT + SPACE + SQLStatements.MAX + BL + 
				Schema.ORDERS_TABLE + "." + ORDERS.TRANSACTION + BR + SPACE + 
				SQLStatements.FROM + SPACE + Schema.ORDERS_TABLE + SPACE + 
				SQLStatements.WHERE + SPACE + 
				Schema.ORDERS_TABLE + "." + ORDERS.CUSTOMER + EQ + 
				Schema.CUSTOMER_TABLE + "." + CUSTOMER.ID + BR + SPACE +
				SQLStatements.AND + SPACE + SQLStatements.DATEDIFF + BL + 
				TRANSACTION.DATE + COMMA + todayDate + BR + SQLStatements.LESS
				+ "10" + BR;
		
		try {
			
			System.out.println(command);
			Statement getDelinquents = connection.createStatement();
			ResultSet results = getDelinquents.executeQuery(command);
			return results;
		}
		
		catch (SQLException e) {
			System.out.println(e.toString());
			return null;
		}
	}
	
	/**
	 * Set a product's sale price
	 * @param productId
	 * @param salePrice
	 * @return 1 on success, -1 on failure
	 */
	public int setItemOnSale (int productId, double salePrice) {
		
		String command = SQLStatements.UPDATE + SPACE + Schema.PRODUCTS_TABLE +
				SPACE + SQLStatements.SET + SPACE + PRODUCTS.SALE_PRICE + EQ +
				salePrice + SPACE + SQLStatements.WHERE + SPACE + 
				PRODUCTS.ID + EQ + productId;
		
		try {
			
			System.out.println(command);
			Statement removeSales = connection.createStatement();
			removeSales.executeUpdate(command);
			return 1;
		}
		
		catch (SQLException e) {
			System.err.println(e.toString());
			return -1;
		}			
	}
	
	
	/**
	 * Return the items from products table which have a sale price
	 * @return ResultSet / cursor of rows with the following attributes: 
	 * ProductID, Product Name, Product Category, Regular Price, Sale Price. 
	 */
	public ResultSet getItemsOnSale() {
		
		String command = SQLStatements.SELECT + SPACE + PRODUCTS.ID + COMMA +
				PRODUCTS.NAME + COMMA + PRODUCTS.CATEGORY + COMMA +
				PRODUCTS.PRICE + COMMA + PRODUCTS.SALE_PRICE + SPACE + 
				SQLStatements.FROM + SPACE + Schema.PRODUCTS_TABLE + SPACE +
				SQLStatements.WHERE + SPACE + PRODUCTS.SALE_PRICE + 
				SQLStatements.GREATER + "0.0";
		
		try {
			
			System.out.println(command);
			Statement getSaleItem = connection.createStatement();
			ResultSet results = getSaleItem.executeQuery(command);
			return results;
		}
		
		catch (SQLException e) {
			System.out.println(e.toString());
			return null;
		}
	}
	
	/** Remove all sale items in products by setting the sale price null 
	 * @return 1 on success, -1 on failure
	 */
	public int removeAllSaleItems() {
		
		String command = SQLStatements.UPDATE + SPACE + Schema.PRODUCTS_TABLE +
				SPACE + SQLStatements.SET + SPACE + PRODUCTS.SALE_PRICE + EQ +
				SQLStatements.NULL;
		
		try {
			
			System.out.println(command);
			Statement removeSales = connection.createStatement();
			removeSales.executeUpdate(command);
			return 1;
		}
		
		catch (SQLException e) {
			System.err.println(e.toString());
			return -1;
		}			
	}
	
	public void topPurchase(int customerID) {
		
		String command = SQLStatements.SELECT + SPACE + Schema.ORDER_ITEMS_TABLE 
				+ "." + ORDER_ITEMS.PRODUCT_ID + SPACE + SQLStatements.FROM + SPACE +
				Schema.ORDER_ITEMS_TABLE + SPACE + SQLStatements.WHERE + SPACE +
				Schema.ORDER_ITEMS_TABLE + "." + ORDER_ITEMS.TRANSACTION_ID +
				SPACE + SQLStatements.IN 
				+  SPACE + BL +
				
				SQLStatements.SELECT + SPACE + ORDERS.TRANSACTION + SPACE +
				SQLStatements.FROM + SPACE + Schema.ORDERS_TABLE + SPACE +
				SQLStatements.WHERE + SPACE + ORDERS.CUSTOMER + EQ + customerID + BR +
				
				SPACE + SQLStatements.GROUP + SPACE + Schema.ORDER_ITEMS_TABLE 
				+ "." + ORDER_ITEMS.PRODUCT_ID + SPACE + "ORDER BY "
				+ SQLStatements.COUNTROWS + "DESC";
		
		try {
			
			System.out.println(command);
			Statement getSaleItem = connection.createStatement();
			ResultSet results = getSaleItem.executeQuery(command);
			int c = 0;
			String format = "%20d%20s%20s%20.2f";
			String headerFormat = "%20s%20s%20s%20s";
		
				
				System.out.format
					(headerFormat, "Product ID", "Name", "Category",
							"Price");
				System.out.println();
			
			while (results.next() && c < 3) {
				
				//print products
				c++;
				int pid =  results.getInt(1);
				
				String command2 = SQLStatements.SELECT + SPACE + PRODUCTS.NAME +
						COMMA + PRODUCTS.CATEGORY + COMMA + PRODUCTS.PRICE + SPACE +
						SQLStatements.FROM + SPACE + Schema.PRODUCTS_TABLE + SPACE 
						+ SQLStatements.WHERE + SPACE + PRODUCTS.ID + EQ + pid;
				
				getSaleItem = connection.createStatement();
				ResultSet product = getSaleItem.executeQuery(command2);
				
				while (product.next()) {
					
					String name = product.getString(1);
					double deliveryCharge = product.getDouble(3);
					String cat = product.getString(2);
					
					System.out.format(format, pid, name, cat, deliveryCharge);
					System.out.println();
				}
			}
			if (c == 0){
				System.out.println("We cannot suggest any based on your empty pathetic history");
			}
		}
		
		catch (SQLException e) {
			System.out.println(e.toString());
		}
	}
	
public void topPurchaseALL() {
		
		worstProducts("DESC");
	}
	
	public ResultSet todayTransaction (String today) {
		
		today = QE + today + QE;
		
		String command = SQLStatements.SELECT + SPACE + TRANSACTION.ID + COMMA +
				TRANSACTION.STATUS + COMMA + TRANSACTION.STANDBY + COMMA + 
				TRANSACTION.PRICE + COMMA + TRANSACTION.DILIVERY_CHARGE + SPACE 
				+ SQLStatements.FROM + SPACE + Schema.TRANSACTION_TABLE + 
				SPACE + SQLStatements.WHERE + SPACE +
				 TRANSACTION.DATE + EQ + today;
		
		try {
			
			System.out.println(command);
			Statement trans = connection.createStatement();
			ResultSet product = trans.executeQuery(command);
			
			return product;
		}
		
		catch (SQLException e){
			System.err.println(e.toString());
			return null;
		}
	}
	
public void worstProducts(String order) {
		
		String command = 
				SQLStatements.SELECT + SPACE + Schema.PRODUCTS_TABLE + "." +
				PRODUCTS.ID + SPACE + SQLStatements.FROM + SPACE + Schema.PRODUCTS_TABLE +
				COMMA + BL +
				SQLStatements.SELECT + SPACE + Schema.ORDER_ITEMS_TABLE 
				+ "." + ORDER_ITEMS.PRODUCT_ID + " as pid" + COMMA + SQLStatements.COUNTROWS +
				SPACE + "as total_count" + SPACE + SQLStatements.FROM + SPACE +
				Schema.ORDER_ITEMS_TABLE + SPACE + 
				
				
				
				SPACE + SQLStatements.GROUP + SPACE + Schema.ORDER_ITEMS_TABLE 
				+ "." + ORDER_ITEMS.PRODUCT_ID + SPACE +
				
				BR +  " as table2" + SPACE +
				
				SQLStatements.WHERE + SPACE + Schema.PRODUCTS_TABLE + "." +
				PRODUCTS.ID + EQ + "table2.pid" +
				
				" ORDER BY total_count*" + Schema.PRODUCTS_TABLE + "." + 
				PRODUCTS.PRICE + SPACE + order;
		
		try {
			
			System.out.println(command);
			Statement getSaleItem = connection.createStatement();
			ResultSet results = getSaleItem.executeQuery(command);
			int c = 0;
			String format = "%20d%20s%20s%20.2f";
			String headerFormat = "%20s%20s%20s%20s";
		
				
				System.out.format
					(headerFormat, "Product ID", "Name", "Category",
							"Price");
				System.out.println();
			
			while (results.next() && c < 3) {
				
				//print products
				c++;
				int pid =  results.getInt(1);
				
				String command2 = SQLStatements.SELECT + SPACE + PRODUCTS.NAME +
						COMMA + PRODUCTS.CATEGORY + COMMA + PRODUCTS.PRICE + SPACE +
						SQLStatements.FROM + SPACE + Schema.PRODUCTS_TABLE + SPACE 
						+ SQLStatements.WHERE + SPACE + PRODUCTS.ID + EQ + pid;
				
				getSaleItem = connection.createStatement();
				ResultSet product = getSaleItem.executeQuery(command2);
				
				while (product.next()) {
					
					String name = product.getString(1);
					double deliveryCharge = product.getDouble(3);
					String cat = product.getString(2);
					
					System.out.format(format, pid, name, cat, deliveryCharge);
					System.out.println();
				}
			}
			if (c == 0){
				System.out.println("We cannot suggest any based on your empty pathetic history");
			}
		}
		
		catch (SQLException e) {
			System.out.println(e.toString());
		}
	}
	
	
	


}