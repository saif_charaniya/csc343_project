package data_managers;
/**
 * SQLStartUp.java:
 * 
 * Connect to sql server and initialize data
 * @author saif
 *
 */

import java.sql.*;

public class SQLStartUp {
	
	private static final String SQL_URL = "jdbc:mysql://127.0.0.1";
	private static final String DB_CLASS = "com.mysql.jdbc.Driver";
	private static final String USER_NAME = "root";
	private static final String USER_PASSWORD = "";
	private static Connection conn = null;
	private static Statement st = null;
	private static boolean connected = false;
	private static Commands databaseCommands;
	
	/** 
	 * Connect to MySQL Sever, check if db exists, if not create it
	 * @return false if initialization runs into a problem
	 * 		   true if initialization successful
	 */
	public static boolean initialize () {
		
		try {
			
			connect(SQL_URL);
			boolean dbExists = checkDBExists(Schema.DATABASE_NAME);
			if (!dbExists) {
				
				System.out.println("Correct Database Not Found");
				System.out.println("Creating Database");
				
				//create db for program
				if (Database.createDatabase(conn) > 0) {
					System.out.println("Database creared");
				}
				
				else {
					System.out.println("Database not created");
					connected = false;
					return connected;
				}
				
				//connect to db
				connect(SQL_URL + "/" + Schema.DATABASE_NAME);

				//create tables
				if (Database.createTables(conn) > 0) {
					System.out.println("Tables created successfully");
				}
				
				else {
					System.out.println("Problem creating tables");
					connected = false;
					return connected;
				}
				
				connected = true;
				System.out.println("Prepopulating with bulk data");
				createCommands();
				Prepopulate();
				return connected;
			}
			
			else {
				
				//connect to database since it already exists
				connect(SQL_URL + "/" + Schema.DATABASE_NAME);
				connected = true;
				System.out.println("Connected to the existing database!");
				createCommands();
			}
		}
		
		catch (Exception e) {
			System.err.println("Can't connect to server");
			connected = false;
			return connected;
		}
		
		return connected;
	}
	
	/**
	 * Return the Commands object to be used in calling SQL commands
	 * in the database.
	 * @return Commands databaseCommand if connected = true.
	 */
	public static Commands getCommands () {
		
		if (connected) {
			return databaseCommands;
		}
		
		else {
			return null;
		}
	}
	
	/**
	 * Initialize Commands variable
	 */
	public static void createCommands() {
		
		if (conn != null){
			databaseCommands = new Commands(conn);
		}
	}
	
	/** Prepopulate the database using the Prepopulate class **/
	private static void Prepopulate () {
		
		Prepopulate pop = new Prepopulate(databaseCommands);
		if (pop.addAll() == -1) {
			System.out.println("Could not prepopulate tables!");
		}
	}
	
	/** personalized testing of commands. **/
	private static void tests(){
		
		createCommands();
		
		
		databaseCommands.createCar(1, "BMW", "M3 GTR", "WHITE", "LMNOPQ", 1.25);
	}
	
	/**
	 * Check if database with name 'dbName' exists. 
	 * @param dbName
	 * @return True if database exists, else false
	 * @throws SQLException 
	 */
	private static boolean checkDBExists(String dbName) throws SQLException{

		ResultSet resultSet = conn.getMetaData().getCatalogs();
	
		while (resultSet.next()) {
		  String databaseName = resultSet.getString(1);
		  if (databaseName.equals(dbName)) {
			  resultSet.close();
			  return true;
		  }
		}
		
		return false;
	}

	/**
	 * Initialize connection to MySQL
	 * @param cred
	 * @return
	 * @throws ClassNotFoundException
	 */
	public static boolean connect (String url) throws ClassNotFoundException {
		
		Class.forName(DB_CLASS);
		
		try {
			
			conn = DriverManager.getConnection(url, USER_NAME, USER_PASSWORD);
			st = conn.createStatement();
			
		} catch (SQLException e) {
			
			System.err.println("Connection could not be established!");
			e.printStackTrace();
			return false;
		}
		
		return true;
	}
	
	/**
	 * Close connection to MySQL database
	 */
	public void disconnect() {
		
		try {
			
			st.close();
			conn.close();
			
		} catch (SQLException e) {
			
			System.err.println("Exception occured while disconnecting!");
			e.printStackTrace();
			
		} finally {
			
			st = null;
			conn = null;
			
		}
	}
}
