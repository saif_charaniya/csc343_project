package data_managers;

import java.io.File;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import data_managers.Schema.ADDRESS;
import data_managers.Schema.CASH;
import data_managers.Schema.CHEQUE;
import data_managers.Schema.CREDIT_CARD;
import data_managers.Schema.CUSTOMER;
import data_managers.Schema.CUSTOMER_ADDRESS;
import data_managers.Schema.DRIVER;
import data_managers.Schema.EMPLOYEE;
import data_managers.Schema.EMPLOYEE_ADDRESS;
import data_managers.Schema.HAS_CAR;
import data_managers.Schema.HAS_PAYMENT;
import data_managers.Schema.PAYMENT_METHOD;
import data_managers.Schema.PRODUCTS;
import data_managers.Schema.VICHELE;

/**
 * Used to prepopulate the database tables
 * 
 *
 */
public class Prepopulate {

	private Commands dbCommands;
	private Connection connection;
	private static final String SPACE = SQLStatements.SPACE;
	private static final String COMMA = SQLStatements.COMMA;
	private static final String BR = SQLStatements.BR;
	private static final String BL = SQLStatements.BL;
	private static final String EQ = SQLStatements.EQ;
	private static final String QE = "\'";

	public Prepopulate (Commands dbCommands){
		
		this.dbCommands = dbCommands;
		this.connection = dbCommands.connection;
	}
	
	/**
	 * populate all tables
	 */
	public int addAll () {
		
		//add customers
		if (addCustomers() == -1) return -1;
		
		//add drivers
		if(addDrivers() == -1) return -1;
		
		//add products
		if (addProducts() == -1) return -1;
		
		//add transactions
		if (addTransactions() == -1) return -1;
		
		//on success
		return 1;
	}
	
	/**
	 * add customers to db, along with their addresses, and fill 
	 * customer_address table as well. Give the customers payments methods
	 * of either cash credit or cheque and connect them using the has payment 
	 * table
	 *@return 1 on success, -1 on failure
	 */
	public int addCustomers() {
		
		//add customers
		File customerFile = new File("src/prepopulate_data/customers.txt");
		String command = SQLStatements.LOAD + SPACE + SQLStatements.DATA + SPACE
				+ SQLStatements.LOCAL + SPACE + SQLStatements.INFILE 
				+ SPACE + QE + 
				customerFile.getAbsolutePath() + QE + SPACE + SQLStatements.INTO
				+ SPACE + SQLStatements.TABLE
				+ SPACE + Schema.CUSTOMER_TABLE  + SPACE + SQLStatements.FIELDS 
				+ SPACE + SQLStatements.TERMINATEDBY + SPACE + "\',\'" + SPACE 
				+ BL + CUSTOMER.FNAME +
				COMMA + CUSTOMER.LNAME + COMMA + CUSTOMER.GENDER + COMMA +
				CUSTOMER.AGE + COMMA + CUSTOMER.ACC_BALANCE + BR;
		
		//add their addresses
		File addressFile = new File
				("src/prepopulate_data/customer_address.txt");
		String command2 = SQLStatements.LOAD + SPACE + SQLStatements.DATA + SPACE
				+ SQLStatements.LOCAL + SPACE + SQLStatements.INFILE 
				+ SPACE + QE + 
				addressFile.getAbsolutePath() + QE + SPACE + SQLStatements.INTO
				+ SPACE + SQLStatements.TABLE + SPACE + 
				Schema.ADDRESS_TABLE + SPACE + 
				SQLStatements.FIELDS + SPACE + SQLStatements.TERMINATEDBY + 
				SPACE + "\',\'" + SPACE + BL + ADDRESS.COUNTRY + 
				COMMA + ADDRESS.STATE + COMMA + ADDRESS.CITY + COMMA + 
				ADDRESS.STREET + COMMA + ADDRESS.STREET_NUMBER + COMMA + 
				ADDRESS.UNIT_NUMBER + COMMA + ADDRESS.POSTAL_CODE + COMMA + 
				ADDRESS.DELIVERY_CHARGE + BR;
		
		//connect customers to address
		File hasAddressFile = new File
				("src/prepopulate_data/customer_has_address.txt");
		String command3 = SQLStatements.LOAD + SPACE + SQLStatements.DATA + 
				SPACE
				+ SQLStatements.LOCAL + SPACE + SQLStatements.INFILE 
				+ SPACE + QE + 
				hasAddressFile.getAbsolutePath() + QE + SPACE + 
				SQLStatements.INTO
				+ SPACE + SQLStatements.TABLE + SPACE + 
				Schema.CUSTOMER_ADDRESS_TABLE + SPACE + 
				SQLStatements.FIELDS + SPACE + SQLStatements.TERMINATEDBY + 
				SPACE + "\',\'" + SPACE + BL + 
				CUSTOMER_ADDRESS.CUSTOMER + COMMA + CUSTOMER_ADDRESS.STREET +
				COMMA + CUSTOMER_ADDRESS.STREET_NUMBER + COMMA + 
				CUSTOMER_ADDRESS.UNIT_NUMBER + COMMA +
				CUSTOMER_ADDRESS.POSTAL_CODE + BR; 
		
		//add their payment methods
		File paymentFile = new File("src/prepopulate_data/payments.txt");
		String command4 = SQLStatements.LOAD + SPACE + SQLStatements.DATA + 
				SPACE
				+ SQLStatements.LOCAL + SPACE + SQLStatements.INFILE 
				+ SPACE + QE + 
				paymentFile.getAbsolutePath() + QE + SPACE + 
				SQLStatements.INTO
				+ SPACE + SQLStatements.TABLE + SPACE + 
				Schema.PAYMENT_METHOD_TABLE + SPACE + 
				SQLStatements.FIELDS + SPACE + SQLStatements.TERMINATEDBY + 
				SPACE + "\',\'" + SPACE + BL + PAYMENT_METHOD.TYPE + BR;

		//add cash payments
		File cashFile = new File("src/prepopulate_data/cash.txt");
		String command5 = SQLStatements.LOAD + SPACE + SQLStatements.DATA + 
				SPACE
				+ SQLStatements.LOCAL + SPACE + SQLStatements.INFILE 
				+ SPACE + QE + 
				cashFile.getAbsolutePath() + QE + SPACE + 
				SQLStatements.INTO
				+ SPACE + SQLStatements.TABLE + SPACE + Schema.CASH_TABLE + 
				SPACE + SQLStatements.FIELDS + SPACE + 
				SQLStatements.TERMINATEDBY + SPACE + "\',\'" + SPACE + BL + 
				CASH.PAYMENT_METHOD_ID + BR;
		
		//add cheque payments
		File chequeFile = new File("src/prepopulate_data/cheque.txt");
		String command6 = SQLStatements.LOAD + SPACE + SQLStatements.DATA + 
				SPACE
				+ SQLStatements.LOCAL + SPACE + SQLStatements.INFILE 
				+ SPACE + QE + 
				chequeFile.getAbsolutePath() + QE + SPACE + 
				SQLStatements.INTO
				+ SPACE + SQLStatements.TABLE + SPACE + Schema.CHEQUE_TABLE +
				SPACE + SQLStatements.FIELDS + SPACE + 
				SQLStatements.TERMINATEDBY + SPACE + "\',\'" + SPACE + BL + 
				CHEQUE.PAYMENT_METHOD_ID + COMMA + CHEQUE.ACCOUNT_NUMBER + COMMA
				+ CHEQUE.BALANCE_LIMIT + BR;
		
		//add credit payments
		File creditFile = new File("src/prepopulate_data/credit.txt");
		String command7 = SQLStatements.LOAD + SPACE + SQLStatements.DATA + 
				SPACE
				+ SQLStatements.LOCAL + SPACE + SQLStatements.INFILE 
				+ SPACE + QE + 
				creditFile.getAbsolutePath() + QE + SPACE + 
				SQLStatements.INTO
				+ SPACE + SQLStatements.TABLE + SPACE + Schema.CREDIT_CARD_TABLE
				+ SPACE + SQLStatements.FIELDS + SPACE + 
				SQLStatements.TERMINATEDBY + SPACE + "\',\'" + SPACE + BL +
				CREDIT_CARD.PAYMENT_METHOD_ID + COMMA + CREDIT_CARD.CARD_NUMBER 
				+ COMMA + CREDIT_CARD.CARD_EXP_DATE + COMMA + 
				CREDIT_CARD.CARD_CCV + BR;
		
		//connect payments to customers
		File hasPaymentFile = new File ("src/prepopulate_data/has_payment.txt");
		String command8 = SQLStatements.LOAD + SPACE + SQLStatements.DATA + 
				SPACE
				+ SQLStatements.LOCAL + SPACE + SQLStatements.INFILE 
				+ SPACE + QE + 
				hasPaymentFile.getAbsolutePath() + QE + SPACE + 
				SQLStatements.INTO
				+ SPACE + SQLStatements.TABLE + SPACE + Schema.HAS_PAYMENT_TABLE
				+ SPACE + SQLStatements.FIELDS + SPACE + 
				SQLStatements.TERMINATEDBY + SPACE + "\',\'" + SPACE + BL +
				HAS_PAYMENT.CUSTOMER + COMMA + HAS_PAYMENT.PAYMENT_METHOD + BR;
		
		
		try {
			
			System.out.println(command);
			Statement addNewCustomer = this.connection.createStatement();
			addNewCustomer.executeUpdate(command);
			
			System.out.println(command2);
			Statement addAddress= this.connection.createStatement();
			addAddress.executeUpdate(command2);
			
			System.out.println(command3);
			Statement addHasAddress = this.connection.createStatement();
			addHasAddress.executeUpdate(command3);
			
			System.out.println(command4);
			Statement addPayment = this.connection.createStatement();
			addPayment.executeUpdate(command4);
			
			System.out.println(command5);
			Statement addCash = this.connection.createStatement();
			addCash.executeUpdate(command5);
			
			System.out.println(command6);
			Statement addCheque = this.connection.createStatement();
			addCheque.executeUpdate(command6);
			
			System.out.println(command7);
			Statement addCredit = this.connection.createStatement();
			addCredit.executeUpdate(command7);
			
			System.out.println(command8);
			Statement addHasPayment = this.connection.createStatement();
			addHasPayment.executeUpdate(command8);
			
			return 1;
		}
		
		catch (SQLException e) {
			System.err.println(e.toString());
			return -1;
		}
	}
	
	/**
	 * add drivers to db: First add employees, then add drivers, add their 
	 * addresses, connect via employee_address table, add vicheles, connected 
	 * via has_vichele table.
	 *@return 1 on success, -1 on failure;
	 */
	public int addDrivers() {
		
		//add employees command
		File employeeFile = new File("src/prepopulate_data/employees.txt");
		String command1 = SQLStatements.LOAD + SPACE + SQLStatements.DATA + SPACE
				+ SQLStatements.LOCAL + SPACE + SQLStatements.INFILE 
				+ SPACE + QE + 
				employeeFile.getAbsolutePath() + QE + SPACE + SQLStatements.INTO
				+ SPACE + SQLStatements.TABLE + SPACE + Schema.EMPLOYEE_TABLE +
				SPACE + SQLStatements.FIELDS + SPACE + 
				SQLStatements.TERMINATEDBY + SPACE + "\',\'" + SPACE + BL + 
				EMPLOYEE.FNAME + COMMA + EMPLOYEE.LNAME + COMMA + 
				EMPLOYEE.SALARY + BR;
		
		//add the drivers
		File driversFile = new File("src/prepopulate_data/drivers.txt");
		String command2 = SQLStatements.LOAD + SPACE + SQLStatements.DATA + SPACE
				+ SQLStatements.LOCAL + SPACE + SQLStatements.INFILE 
				+ SPACE + QE + 
				driversFile.getAbsolutePath() + QE + SPACE + SQLStatements.INTO
				+ SPACE + SQLStatements.TABLE + SPACE + Schema.DRIVER_TABLE +
				SPACE + SQLStatements.FIELDS + SPACE + 
				SQLStatements.TERMINATEDBY + SPACE + "\',\'" + SPACE + BL + 
				DRIVER.EMPLOYEE_ID + COMMA + DRIVER.CURRENT_LOCATION + BR;
		
		//add the driver addresses
		File driverAddressFile = new File
				("src/prepopulate_data/employee_address.txt");
		String command3 = SQLStatements.LOAD + SPACE + SQLStatements.DATA + SPACE
				+ SQLStatements.LOCAL + SPACE + SQLStatements.INFILE 
				+ SPACE + QE + 
				driverAddressFile.getAbsolutePath() + QE + SPACE + 
				SQLStatements.INTO
				+ SPACE + SQLStatements.TABLE + SPACE + Schema.ADDRESS_TABLE +
				SPACE +  SQLStatements.FIELDS + SPACE + 
				SQLStatements.TERMINATEDBY + 
				SPACE + "\',\'" + SPACE + BL + ADDRESS.COUNTRY + 
				COMMA + ADDRESS.STATE + COMMA + ADDRESS.CITY + COMMA + 
				ADDRESS.STREET + COMMA + ADDRESS.STREET_NUMBER + COMMA + 
				ADDRESS.UNIT_NUMBER + COMMA + ADDRESS.POSTAL_CODE + BR;
		
		
		//add employee_address  tables to connect address with employee
		File hasAddressFile = new File
				("src/prepopulate_data/employee_has_address.txt");
		String command4 = SQLStatements.LOAD + SPACE + 
				SQLStatements.DATA + SPACE
				+ SQLStatements.LOCAL + SPACE + SQLStatements.INFILE 
				+ SPACE + QE + 
				hasAddressFile.getAbsolutePath() + QE + SPACE + 
				SQLStatements.INTO
				+ SPACE + SQLStatements.TABLE + SPACE + 
				Schema.EMPLOYEE_ADDRESS_TABLE + SPACE + SQLStatements.FIELDS + 
				SPACE + SQLStatements.TERMINATEDBY + 
				SPACE + "\',\'" + SPACE + BL + EMPLOYEE_ADDRESS.EMPLOYEE + COMMA
				+ EMPLOYEE_ADDRESS.STREET + COMMA + 
				EMPLOYEE_ADDRESS.STREET_NUMBER + COMMA + 
				EMPLOYEE_ADDRESS.UNIT_NUMBER + COMMA +
				EMPLOYEE_ADDRESS.POSTAL_CODE + BR;
		
		//add cars to vichele table
		File carsFile = new File("src/prepopulate_data/cars.txt");
		String command5 = SQLStatements.LOAD + SPACE + 
				SQLStatements.DATA + SPACE
				+ SQLStatements.LOCAL + SPACE + SQLStatements.INFILE 
				+ SPACE + QE + 
				carsFile.getAbsolutePath() + QE + SPACE + 
				SQLStatements.INTO
				+ SPACE + SQLStatements.TABLE + SPACE + Schema.VICHELE_TABLE + 
				SPACE + SQLStatements.FIELDS + SPACE + 
				SQLStatements.TERMINATEDBY + SPACE + "\',\'" + SPACE + BL + 
				VICHELE.MAKE + COMMA + VICHELE.MODEL + COMMA + VICHELE.COLOUR +
				COMMA + VICHELE.LICENSE + COMMA + VICHELE.CPM + BR;
		
		//connect cars to drivers via has_vichele table
		File hasCarFile = new File("src/prepopulate_data/driver_has_car.txt");
		String command6 = SQLStatements.LOAD + SPACE + 
				SQLStatements.DATA + SPACE
				+ SQLStatements.LOCAL + SPACE + SQLStatements.INFILE 
				+ SPACE + QE + 
				hasCarFile.getAbsolutePath() + QE + SPACE + 
				SQLStatements.INTO
				+ SPACE + SQLStatements.TABLE + SPACE + Schema.HAS_CAR_TABLE + 
				SPACE + SQLStatements.FIELDS + SPACE + 
				SQLStatements.TERMINATEDBY + SPACE + "\',\'" + SPACE + BL + 
				HAS_CAR.DRIVER + COMMA + HAS_CAR.VICHELE + BR;
		
		//Run commands:
		try {
			//add customers
			System.out.println(command1);
			Statement addEmployees = this.connection.createStatement();
			addEmployees.executeUpdate(command1);
			
			System.out.println(command2);
			Statement addDrivers= this.connection.createStatement();
			addDrivers.executeUpdate(command2);
			
			System.out.println(command3);
			Statement addAddress = this.connection.createStatement();
			addAddress.executeUpdate(command3);
			
			System.out.println(command4);
			Statement addHasAddress = this.connection.createStatement();
			addHasAddress.executeUpdate(command4);
			
			System.out.println(command5);
			Statement addCars = this.connection.createStatement();
			addCars.executeUpdate(command5);
			
			System.out.println(command6);
			Statement addHasCar = this.connection.createStatement();
			addHasCar.executeUpdate(command6);
			
			return 1;
		}
		
		catch (SQLException e) {
			System.err.println(e.toString());
			return -1;
		}	
	}
	
	/**
	 * add products to db
	 */
	public int addProducts() {
		
		//add products
		//add employees command
		File productsFile = new File("src/prepopulate_data/products.txt");
		String command1 = SQLStatements.LOAD + SPACE + SQLStatements.DATA + SPACE
				+ SQLStatements.LOCAL + SPACE + SQLStatements.INFILE 
				+ SPACE + QE + 
				productsFile.getAbsolutePath() + QE + SPACE + SQLStatements.INTO
				+ SPACE + SQLStatements.TABLE + SPACE + Schema.PRODUCTS_TABLE +
				SPACE + SQLStatements.FIELDS + SPACE + 
				SQLStatements.TERMINATEDBY + SPACE + "\',\'" + SPACE + BL + 
				PRODUCTS.NAME + COMMA + PRODUCTS.DESCRIPTION + COMMA + 
				PRODUCTS.SUPPLIER + COMMA + PRODUCTS.MANUFACTURER + COMMA +
				PRODUCTS.CATEGORY + COMMA + PRODUCTS.PRICE + COMMA + 
				PRODUCTS.TAXABLE + COMMA + PRODUCTS.SALE_PRICE + BR;
		try {
			
			System.out.println(command1);
			Statement addProducts = this.connection.createStatement();
			addProducts.executeUpdate(command1);
		
			return 1;
		}
	
		catch (SQLException e) {
			System.err.println(e.toString());
			return -1;
		}	
	}
	
	/**
	 * Add multiple transactions to the database using the addTransaction
	 * method from Commands
	 * @return 1 on success, -1 on failure
	 */
	public int addTransactions() {
		
		//product lists for transactions
		ArrayList<Integer> plist1 = new ArrayList<Integer>();
		ArrayList<Integer> plist2 = new ArrayList<Integer>();
		ArrayList<Integer> plist3 = new ArrayList<Integer>();
		ArrayList<Integer> plist4 = new ArrayList<Integer>();
		ArrayList<Integer> plist5 = new ArrayList<Integer>();
		
		//items in each product list
		
		plist1.add(1);
		plist1.add(5);
		plist1.add(6);
		plist1.add(7);
		plist1.add(7);
		plist1.add(7);
		plist1.add(7);
		plist1.add(8);
		
		plist2.add(2);
		plist2.add(3);
		plist2.add(4);
		plist2.add(5);
		plist2.add(6);
		plist2.add(8);
		plist2.add(9);
		plist2.add(8);
		
		plist3.add(1);
		plist3.add(1);
		plist3.add(1);
		plist3.add(1);
		plist3.add(3);
		plist3.add(3);
		plist3.add(3);
		plist3.add(3);
		plist3.add(10);
		plist3.add(10);
		plist3.add(10);
		plist3.add(10);
		plist3.add(10);
		plist3.add(10);
		plist3.add(11);
		plist3.add(11);
		plist3.add(11);
		plist3.add(11);
		plist3.add(11);
		plist3.add(11);
		plist3.add(11);
		plist3.add(11);

		plist4.add(11);
		plist4.add(11);
		plist4.add(11);
		plist4.add(10);
		plist4.add(6);
		plist4.add(6);
		plist4.add(6);
		plist4.add(6);
		plist4.add(2);
		plist4.add(2);
		plist4.add(2);
		plist4.add(1);
		plist4.add(1);
		plist4.add(1);
		plist4.add(3);
		plist4.add(3);
		plist4.add(4);
		plist4.add(4);
		plist4.add(5);
		plist4.add(5);
		plist4.add(5);
		plist4.add(5);
		
		plist5.add(1);
		plist5.add(1);
		plist5.add(1);
		plist5.add(3);
		plist5.add(3);
		plist5.add(3);
		plist5.add(5);
		plist5.add(5);
		plist5.add(5);
		plist5.add(7);
		plist5.add(7);
		plist5.add(7);
		plist5.add(7);
		plist5.add(7);
		plist5.add(7);
		plist5.add(7);
		plist5.add(7);
		plist5.add(7);
		plist5.add(9);
		plist5.add(9);
		plist5.add(9);
		plist5.add(9);
		
		//set dates for transaction
		ArrayList<String> dates = new ArrayList<String>();
		dates.add("2015-08-01");
		dates.add("2015-08-02");
		dates.add("2015-08-03");
		
		//record transactions into the database
		ArrayList<ArrayList<Integer>> all_plists = 
				new ArrayList<ArrayList<Integer>>();
		
		all_plists.add(plist1);
		all_plists.add(plist2);
		all_plists.add(plist3);
		all_plists.add(plist4);
		all_plists.add(plist5);
		
		int len_plist = all_plists.size();
		int len_dates = dates.size();
		int len_customer = 8;
		
		//record transactions
		for (int i = 0; i < 25; i++) {
			
			int customerID = (i % len_customer) + 1;
			String this_date = dates.get(i % len_dates);
			ArrayList<Integer> this_plist = all_plists.get(i % len_plist);
			
			int status = dbCommands.addOrder
					(customerID, this_plist, this_date, "null");
			
			if (status == -1) {
				return -1;
			}
		}
		
		return 1;
	}

}
