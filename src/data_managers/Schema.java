package data_managers;

import data_managers.SQLStatements;

public interface Schema {
	
	public static final String DATABASE_NAME = "test_db3";//"csc343_project_db";
	
	public static final String SPACE = SQLStatements.SPACE;
	public static final String COMMA = SQLStatements.COMMA;
	public static final String BR = SQLStatements.BR;
	public static final String BL = SQLStatements.BL;
	
	public static final String CUSTOMER_TABLE = "customers";
	public static final String TRANSACTION_TABLE = "transaction";
	public static final String PRODUCTS_TABLE = "products";
	public static final String TIMETABLE_TABLE = "timetable";
	public static final String ADDRESS_TABLE = "address";
	public static final String VICHELE_TABLE = "vichele";
	public static final String EMPLOYEE_TABLE = "employee";
	public static final String DRIVER_TABLE = "driver";
	public static final String EMPLOYEE_ADDRESS_TABLE = "employee_address";
	public static final String CUSTOMER_ADDRESS_TABLE = "customer_address";
	public static final String PAYMENT_METHOD_TABLE = "payment_method";
	public static final String CREDIT_CARD_TABLE = "credit_card_payment";
	public static final String CHEQUE_TABLE = "cheque_payment";
	public static final String CASH_TABLE = "cash_payment";
	public static final String ORDERS_TABLE = "orders";
	public static final String HAS_PAYMENT_TABLE = "has_payment";
	public static final String ORDER_ITEMS_TABLE = "order_items";
	public static final String WORKS_TABLE = "works";
	public static final String DELIVERIES_TABLE = "deliveries";
	public static final String HAS_CAR_TABLE = "has_car";

	public interface CUSTOMER {
		
		public static final String ID = "id";
		public static final String ACTIVE = "active";
		public static final String GENDER  = "gender";
		public static final String FNAME = "fname";
		public static final String LNAME = "lname";
		public static final String AGE = "age";
		public static final String ACC_BALANCE = "acc_balance";

		public static final String CREATE_TABLE =
			SQLStatements.CREATE + SPACE + SQLStatements.TABLE + SPACE +
			SQLStatements.IFNE + SPACE + CUSTOMER_TABLE + SPACE + BL + ID + 
			SPACE + SQLStatements.INT + SPACE + SQLStatements.AUTO_INCREMENT +
			COMMA + ACTIVE + SPACE + SQLStatements.VARCHAR + BL + "20" + BR +
			SPACE + SQLStatements.NOT_NULL + SPACE + SQLStatements.DEFAULT +
			SPACE + "\'active\'" + COMMA + GENDER + SPACE + 
			SQLStatements.VARCHAR + BL + "6" + BR + SPACE + 
			SQLStatements.NOT_NULL + COMMA + FNAME + SPACE + 
			SQLStatements.VARCHAR + BL + "30" + BR + SPACE + 
			SQLStatements.NOT_NULL + COMMA + LNAME + SPACE + 
			SQLStatements.VARCHAR + BL + "30" + BR + SPACE + 
			SQLStatements.NOT_NULL + COMMA + AGE + SPACE + SQLStatements.INT +
			SPACE + SQLStatements.NOT_NULL + COMMA + ACC_BALANCE + SPACE +
			SQLStatements.REAL + SPACE + SQLStatements.NOT_NULL + COMMA
			+ SQLStatements.PRIMARYKEY +
			BL + ID + BR + BR;
	}
	
	public interface TRANSACTION {
		
		public static final String PRICE = "price";
		// tid serves as key. add it to table.
		public static final String TICKET_NUMBER = "tid";
		
		//consider removing delivery charge
		public static final String DILIVERY_CHARGE = "dilivery_charge";
		public static final String ID = "id";
		public static final String DATE = "dilivery_date";
		
		//need these three dates; add them to database.
		public static final String START_TIME = "start_time";
		public static final String ARRIVED_CUSTOMER_TIME = "arrive_customer_time";
		public static final String COME_HOME_TIME = "come_home_time";
		
		public static final String STATUS = "status";
		public static final String STANDBY = "standby";
		//added
		public static final String DRIVER_NAME = "driver_name";
		
		public static final String CREATE_TABLE =
			SQLStatements.CREATE + SPACE + SQLStatements.TABLE + SPACE +
			SQLStatements.IFNE + SPACE + TRANSACTION_TABLE + SPACE + BL + PRICE
			+ SPACE + SQLStatements.REAL + SPACE + SQLStatements.NOT_NULL + 
			COMMA + ID + SPACE + SQLStatements.INT + SPACE + 
			SQLStatements.AUTO_INCREMENT + COMMA + DATE + SPACE +
			SQLStatements.DATE + SPACE + SQLStatements.NOT_NULL + COMMA + 
			DILIVERY_CHARGE + SPACE + SQLStatements.REAL + SPACE + 
			SQLStatements.NOT_NULL + COMMA + STATUS + SPACE + 
			SQLStatements.VARCHAR + BL + "20" + BR + COMMA + STANDBY + SPACE +
			SQLStatements.VARCHAR + BL + "20" + BR + COMMA +
			START_TIME + SPACE +
			SQLStatements.TIME + COMMA +
			ARRIVED_CUSTOMER_TIME + SPACE +
			SQLStatements.TIME + COMMA +
			COME_HOME_TIME + SPACE +
			SQLStatements.TIME + COMMA +
			SQLStatements.PRIMARYKEY + BL + ID
			+ BR + BR;
	}
	
	public interface PRODUCTS {
		
		public static final String CATEGORY = "category";
		public static final String SUPPLIER = "supplier";
		public static final String ID = "id";
		public static final String DESCRIPTION = "description";
		public static final String PRICE = "price";
		public static final String MANUFACTURER = "manufacturer";
		public static final String TAXABLE = "taxable";
		public static final String NAME = "product_name";
		public static final String SALE_PRICE = "sale_price";
		
		public static final String CREATE_TABLE =
			SQLStatements.CREATE + SPACE + SQLStatements.TABLE + SPACE +
			SQLStatements.IFNE + SPACE + PRODUCTS_TABLE + SPACE + BL + PRICE + 
			SPACE + SQLStatements.REAL + SPACE + SQLStatements.NOT_NULL + COMMA 
			+ CATEGORY + SPACE + SQLStatements.VARCHAR + BL + "50" + BR + SPACE 
			+ SQLStatements.NOT_NULL + COMMA + SUPPLIER + SPACE + 
			SQLStatements.VARCHAR + BL + "50" + BR + SPACE + 
			SQLStatements.NOT_NULL + COMMA + ID + SPACE + SQLStatements.INT + 
			SPACE + SQLStatements.AUTO_INCREMENT + COMMA + DESCRIPTION + SPACE +
			SQLStatements.VARCHAR + BL + "250" + BR + SPACE + 
			SQLStatements.NOT_NULL + COMMA + MANUFACTURER + SPACE + 
			SQLStatements.VARCHAR + BL + "50" + BR + SPACE + 
			SQLStatements.NOT_NULL + COMMA + TAXABLE + SPACE + 
			SQLStatements.REAL + COMMA +
			NAME + SPACE + SQLStatements.VARCHAR + BL + "50" + BR + SPACE + 
			SQLStatements.NOT_NULL + COMMA + SALE_PRICE + SPACE + 
			SQLStatements.REAL + COMMA 
			+ SQLStatements.PRIMARYKEY + BL + ID + BR
			+ BR;
	}
	
	public interface TIMETABLE {
		
		public static final String DATE = "work_date";
		public static final String START_TIME = "start_time";
		public static final String END_TIME = "end_time";
		public static final String SHIFT_ID = "id";
		
		public static final String CREATE_TABLE = 
			SQLStatements.CREATE + SPACE + SQLStatements.TABLE + SPACE +
			SQLStatements.IFNE + SPACE + TIMETABLE_TABLE + SPACE + BL + DATE + 
			SPACE + SQLStatements.DATE + COMMA + START_TIME + SPACE + 
			SQLStatements.TIME + COMMA + END_TIME + SPACE + SQLStatements.TIME +
			COMMA + SHIFT_ID + SPACE + SQLStatements.INT + SPACE + 
			SQLStatements.AUTO_INCREMENT + SPACE + COMMA +
			SQLStatements.PRIMARYKEY + BL + SHIFT_ID + BR + BR;
	}
	
	public interface ADDRESS {
		
		public static final String COUNTRY = "country";
		public static final String STATE = "state";
		public static final String CITY = "city";
		public static final String STREET = "street";
		public static final String STREET_NUMBER = "street_number";
		public static final String UNIT_NUMBER = "unit_number";
		public static final String POSTAL_CODE = "postal_code";
		public static final String DELIVERY_CHARGE = "delivery_charge";
		
		public static final String CREATE_TABLE = 
			SQLStatements.CREATE + SPACE + SQLStatements.TABLE + SPACE +
			SQLStatements.IFNE + SPACE + ADDRESS_TABLE + SPACE + BL + 
			COUNTRY + SPACE + SQLStatements.VARCHAR + BL + "50" + BR + SPACE +
			SQLStatements.NOT_NULL + SPACE + SQLStatements.DEFAULT + SPACE +
			"\'Canada\'" + COMMA + STATE + SPACE + SQLStatements.VARCHAR + BL +
			"50" + BR + SPACE + SQLStatements.NOT_NULL + SPACE + 
			SQLStatements.DEFAULT + "\'Ontario\'" + COMMA + CITY + SPACE + 
			SQLStatements.VARCHAR + BL + "50" + BR + SPACE + 
			SQLStatements.NOT_NULL + SPACE + SQLStatements.DEFAULT + SPACE +
			"\'Toronto\'" + COMMA + STREET + SPACE + SQLStatements.VARCHAR + BL
			+ "50" + BR + SPACE + SQLStatements.NOT_NULL + SPACE +
			SQLStatements.DEFAULT + SPACE + "\'Street 1\'" + COMMA + 
			STREET_NUMBER + SPACE + SQLStatements.INT + SPACE + 
			SQLStatements.NOT_NULL + COMMA + UNIT_NUMBER + SPACE + 
			SQLStatements.INT + SPACE + SQLStatements.DEFAULT + SPACE + "-1" + 
			COMMA +  POSTAL_CODE + SPACE + SQLStatements.VARCHAR + BL + "6" + BR
			+ SPACE + SQLStatements.NOT_NULL + COMMA + DELIVERY_CHARGE + SPACE +
			SQLStatements.REAL + COMMA + SQLStatements.PRIMARYKEY
			+ BL + CITY + COMMA + STREET + COMMA + STREET_NUMBER + COMMA +
			UNIT_NUMBER + COMMA + POSTAL_CODE + BR + BR;
	}
	
	public interface VICHELE {
		
		public static final String CPM = "cpm";
		public static final String MAKE = "make";
		public static final String MODEL = "model";
		public static final String LICENSE = "license";
		public static final String COLOUR = "colour";
	
		public static final String CREATE_TABLE = 
			SQLStatements.CREATE + SPACE + SQLStatements.TABLE + SPACE +
			SQLStatements.IFNE + SPACE + VICHELE_TABLE + SPACE + BL + CPM + 
			SPACE + SQLStatements.REAL + SPACE + SQLStatements.NOT_NULL + COMMA
			+ MAKE + SPACE + SQLStatements.VARCHAR + BL + "50" + BR + SPACE +
			SQLStatements.NOT_NULL + COMMA + MODEL + SPACE + 
			SQLStatements.VARCHAR + BL + "50" + BR + SPACE + 
			SQLStatements.NOT_NULL + COMMA + LICENSE + SPACE + 
			SQLStatements.VARCHAR + BL + "10" + BR + SPACE + 
			SQLStatements.NOT_NULL + COMMA + COLOUR + SPACE + 
			SQLStatements.VARCHAR + BL + "10" + BR + COMMA + 
			SQLStatements.PRIMARYKEY + BL + LICENSE + BR + BR;
	}
	
	public interface EMPLOYEE {
		
		public static final String ID = "id";
		public static final String FNAME = "fname";
		public static final String LNAME = "lname";
		public static final String SALARY = "salary";
		
		public static final String CREATE_TABLE = 
			SQLStatements.CREATE + SPACE + SQLStatements.TABLE + SPACE +
			SQLStatements.IFNE + SPACE + EMPLOYEE_TABLE + SPACE + BL + ID + 
			SPACE + SQLStatements.INT + SPACE + SQLStatements.AUTO_INCREMENT + 
			COMMA + FNAME + SPACE + SQLStatements.VARCHAR + BL + "30" + BR + 
			SPACE + SQLStatements.NOT_NULL + COMMA + LNAME + SPACE + 
			SQLStatements.VARCHAR + BL + "30" + BR + SPACE + 
			SQLStatements.NOT_NULL + COMMA + SALARY + SPACE + 
			SQLStatements.REAL + SPACE + SQLStatements.NOT_NULL + COMMA +
			SQLStatements.PRIMARYKEY + BL + ID + BR + BR;
	}
	
	public interface DRIVER {
		
		public static final String CURRENT_LOCATION = "current_location";
		public static final String EMPLOYEE_ID = "employee_id";
		public static final String FREE_WHEN = "free_when";
		
		public static final String CREAT_TABLE = 
			SQLStatements.CREATE + SPACE + SQLStatements.TABLE + SPACE +
			SQLStatements.IFNE + SPACE + DRIVER_TABLE + SPACE + BL + 
			CURRENT_LOCATION + SPACE + SQLStatements.VARCHAR + BL + "20" + BR +
			SPACE + COMMA + EMPLOYEE_ID + SPACE + SQLStatements.INT + SPACE +
			SQLStatements.NOT_NULL + COMMA + SQLStatements.FOREIGN_KEY + 
			SPACE + BL + EMPLOYEE_ID + BR + SPACE + SQLStatements.REFERENCES +
			SPACE + EMPLOYEE_TABLE + SPACE + BL + Schema.EMPLOYEE.ID + BR 
			+ COMMA + FREE_WHEN + SPACE + SQLStatements.TIME + SPACE + 
			SQLStatements.NOT_NULL + SPACE + SQLStatements.DEFAULT + SPACE +
			"\'00:00:00\'" + COMMA + SQLStatements.PRIMARYKEY +
			BL + EMPLOYEE_ID + BR + BR;
	}
	
	public interface EMPLOYEE_ADDRESS {
		 
		public static final String POSTAL_CODE = "postal_code";
		public static final String STREET_NUMBER = "street_number";
		public static final String STREET = "street";
		public static final String UNIT_NUMBER = "unit_number";
		public static final String EMPLOYEE = "employee_id"; 
		
		public static final String CREATE_TABLE = 
			SQLStatements.CREATE + SPACE + SQLStatements.TABLE + SPACE +
			SQLStatements.IFNE + SPACE + EMPLOYEE_ADDRESS_TABLE + SPACE + BL +
			POSTAL_CODE + SPACE + SQLStatements.VARCHAR + BL + "6" + BR + SPACE
			+ SQLStatements.NOT_NULL + SPACE + SQLStatements.REFERENCES + SPACE 
			+ ADDRESS_TABLE + BL + Schema.ADDRESS.POSTAL_CODE + BR + COMMA + 
			STREET_NUMBER + SPACE + SQLStatements.INT + SPACE + 
			SQLStatements.NOT_NULL + SPACE + SQLStatements.REFERENCES + SPACE +
			ADDRESS_TABLE + BL + Schema.ADDRESS.STREET_NUMBER + BR + COMMA +
			STREET + SPACE + SQLStatements.VARCHAR + BL + "50" + BR + SPACE + 
			SQLStatements.NOT_NULL + SPACE + SQLStatements.REFERENCES + SPACE +
			ADDRESS_TABLE + BL + Schema.ADDRESS.STREET + BR + COMMA + 
			UNIT_NUMBER + SPACE + SQLStatements.INT + SPACE + 
			SQLStatements.NOT_NULL + SPACE + SQLStatements.REFERENCES + SPACE +
			ADDRESS_TABLE + BL + Schema.ADDRESS.UNIT_NUMBER + BR + COMMA +
			EMPLOYEE + SPACE + SQLStatements.INT + SPACE +
			SQLStatements.NOT_NULL + SPACE + SQLStatements.REFERENCES + SPACE +
			EMPLOYEE_TABLE + SPACE + BL + Schema.EMPLOYEE.ID + BR + COMMA + 
			SQLStatements.PRIMARYKEY + BL + POSTAL_CODE + COMMA + STREET_NUMBER
			+ COMMA + STREET + COMMA + UNIT_NUMBER + COMMA + EMPLOYEE + BR + BR;
	}
	
	public interface CUSTOMER_ADDRESS {
		 
		public static final String POSTAL_CODE = "postal_code";
		public static final String STREET_NUMBER = "street_number";
		public static final String STREET = "street";
		public static final String UNIT_NUMBER = "unit_number";
		public static final String CUSTOMER = "customer_id"; 
		
		public static final String CREATE_TABLE = 
			SQLStatements.CREATE + SPACE + SQLStatements.TABLE + SPACE +
			SQLStatements.IFNE + SPACE + CUSTOMER_ADDRESS_TABLE + SPACE + BL +
			POSTAL_CODE + SPACE + SQLStatements.VARCHAR + BL + "6" + BR + SPACE
			+ SQLStatements.NOT_NULL + SPACE + SQLStatements.REFERENCES + SPACE
			+ ADDRESS_TABLE + BL + Schema.ADDRESS.POSTAL_CODE + BR + COMMA + 
			STREET_NUMBER + SPACE + SQLStatements.INT + SPACE + 
			SQLStatements.NOT_NULL + SPACE + SQLStatements.REFERENCES + SPACE +
			ADDRESS_TABLE + BL + Schema.ADDRESS.STREET_NUMBER + BR + COMMA +
			STREET + SPACE + SQLStatements.VARCHAR + BL + "50" + BR + SPACE + 
			SQLStatements.NOT_NULL + SPACE + SQLStatements.REFERENCES + SPACE +
			ADDRESS_TABLE + BL + Schema.ADDRESS.STREET + BR + COMMA + 
			UNIT_NUMBER + SPACE + SQLStatements.INT + SPACE + 
			SQLStatements.NOT_NULL + SPACE + SQLStatements.REFERENCES + SPACE +
			ADDRESS_TABLE + BL + Schema.ADDRESS.UNIT_NUMBER + BR + COMMA +
			CUSTOMER + SPACE + SQLStatements.INT + SPACE +
			SQLStatements.NOT_NULL + SPACE + SQLStatements.REFERENCES + SPACE +
			EMPLOYEE_TABLE + SPACE + BL + Schema.EMPLOYEE.ID + BR + COMMA + 
			SQLStatements.PRIMARYKEY + BL + POSTAL_CODE + COMMA + STREET_NUMBER
			+ COMMA + STREET + COMMA + UNIT_NUMBER + COMMA + CUSTOMER + BR + BR;
	}
	
	public interface PAYMENT_METHOD {
		
		public static final String ID = "id";
		public static final String TYPE = "type";
		
		public static final String CREATE_TABLE = 
			SQLStatements.CREATE + SPACE + SQLStatements.TABLE + SPACE +
			SQLStatements.IFNE + SPACE + PAYMENT_METHOD_TABLE + SPACE + BL +
			ID + SPACE + SQLStatements.INT + SPACE + 
			SQLStatements.AUTO_INCREMENT + COMMA + TYPE + SPACE +
			SQLStatements.VARCHAR + BL + "10" + BR + SPACE + 
			SQLStatements.NOT_NULL + COMMA + SQLStatements.PRIMARYKEY + BL +
			ID + BR + BR;
	}
	
	public interface CREDIT_CARD {
		
		public static final String CARD_NUMBER = "number";
		public static final String PAYMENT_METHOD_ID = "payment_id";
		public static final String CARD_EXP_DATE = "exp";
		public static final String CARD_CCV = "ccv";
	
		public static final String CREATE_TABLE = 
			SQLStatements.CREATE + SPACE + SQLStatements.TABLE + SPACE +
			SQLStatements.IFNE + SPACE + CREDIT_CARD_TABLE + SPACE + BL +
			CARD_NUMBER + SPACE + SQLStatements.VARCHAR + BL + "16" + BR + 
			SQLStatements.NOT_NULL + COMMA + 
			PAYMENT_METHOD_ID + SPACE +
			SQLStatements.INT + SPACE + SQLStatements.REFERENCES + SPACE +
			PAYMENT_METHOD_TABLE + SPACE + BL + Schema.PAYMENT_METHOD.ID + BR +
			COMMA + CARD_CCV + SPACE + SQLStatements.VARCHAR + BL + "3" + BR +
			COMMA + CARD_EXP_DATE + SPACE + SQLStatements.DATE + COMMA +
			SQLStatements.UNIQUE + BL + CARD_NUMBER + BR + COMMA +
			SQLStatements.PRIMARYKEY + BL + PAYMENT_METHOD_ID + BR + BR;
	}
	
	public interface CHEQUE {
		
		public static final String ACCOUNT_NUMBER = "account_number";
		public static final String BALANCE_LIMIT = "balance_limit";
		public static final String PAYMENT_METHOD_ID = "payment_id";
		
		public static final String CREATE_TABLE = 
			SQLStatements.CREATE + SPACE + SQLStatements.TABLE + SPACE +
			SQLStatements.IFNE + SPACE + CHEQUE_TABLE + SPACE + BL + 
			ACCOUNT_NUMBER + SPACE + SQLStatements.VARCHAR + BL + "30" + BR + 
			SPACE + SQLStatements.NOT_NULL + COMMA + BALANCE_LIMIT + SPACE + 
			SQLStatements.REAL + SPACE + SQLStatements.NOT_NULL + COMMA +
			PAYMENT_METHOD_ID + SPACE + SQLStatements.INT + SPACE +
			SQLStatements.REFERENCES + SPACE + PAYMENT_METHOD_TABLE + SPACE + BL
			+ Schema.PAYMENT_METHOD.ID + BR + COMMA + SQLStatements.UNIQUE + BL 
			+ ACCOUNT_NUMBER + BR + COMMA + SQLStatements.PRIMARYKEY + BL +
			PAYMENT_METHOD_ID + BR + BR;
	}
	
	public interface CASH {
		
		public static final String PAYMENT_METHOD_ID = "payment_id";
		
		public static final String CREATE_TABLE = 
			SQLStatements.CREATE + SPACE + SQLStatements.TABLE + SPACE +
			SQLStatements.IFNE + SPACE + CASH_TABLE + SPACE + BL + 
			PAYMENT_METHOD_ID + SPACE + SQLStatements.INT + SPACE +
			SQLStatements.REFERENCES + SPACE + PAYMENT_METHOD_TABLE + SPACE + BL
			+ Schema.PAYMENT_METHOD.ID + BR + COMMA + SQLStatements.PRIMARYKEY +
			BL + PAYMENT_METHOD_ID + BR + BR;
	}
	
	public interface ORDERS {
		
		public static final String CUSTOMER = "customer_id";
		public static final String TRANSACTION = "transaction_id";
		
		public static final String CREATE_TABLE = 
			SQLStatements.CREATE + SPACE + SQLStatements.TABLE + SPACE +
			SQLStatements.IFNE + SPACE + ORDERS_TABLE + SPACE + BL + 
			CUSTOMER + SPACE + SQLStatements.INT + SPACE + 
			SQLStatements.REFERENCES + SPACE + CUSTOMER_TABLE + SPACE + BL
			+ Schema.CUSTOMER.ID + BR + COMMA + TRANSACTION + SPACE + 
			SQLStatements.INT + SPACE + SQLStatements.REFERENCES + SPACE + 
			TRANSACTION_TABLE + SPACE + BL + Schema.TRANSACTION.ID + BR + 
			COMMA + SQLStatements.PRIMARYKEY + BL + CUSTOMER + COMMA + 
			TRANSACTION + BR + BR;
	}
	
	public interface HAS_PAYMENT {
		
		public static final String CUSTOMER = "customer_id";
		public static final String PAYMENT_METHOD = "payment_method_id";
		
		public static final String CREATE_TABLE = 
			SQLStatements.CREATE + SPACE + SQLStatements.TABLE + SPACE +
			SQLStatements.IFNE + SPACE + HAS_PAYMENT_TABLE + SPACE + BL + 
			CUSTOMER + SPACE + SQLStatements.INT + SPACE + 
			SQLStatements.REFERENCES + SPACE + CUSTOMER_TABLE + SPACE + BL
			+ Schema.CUSTOMER.ID + BR + COMMA + PAYMENT_METHOD + SPACE + 
			SQLStatements.INT + SPACE + SQLStatements.REFERENCES + SPACE + 
			PAYMENT_METHOD_TABLE + SPACE + BL + Schema.PAYMENT_METHOD.ID + BR + 
			COMMA + SQLStatements.PRIMARYKEY + BL + CUSTOMER + COMMA + 
			PAYMENT_METHOD + BR + BR;
	}
	
	public interface ORDER_ITEMS {
		
		public static final String TRANSACTION_ID = "transaction_id";
		public static final String PRODUCT_ID = "product_id";
		
		public static final String CREATE_TABLE = 
			SQLStatements.CREATE + SPACE + SQLStatements.TABLE + SPACE +
			SQLStatements.IFNE + SPACE + ORDER_ITEMS_TABLE + SPACE + BL + 
			PRODUCT_ID + SPACE + SQLStatements.INT + SPACE + 
			SQLStatements.REFERENCES + SPACE + PRODUCTS_TABLE + SPACE + BL
			+ Schema.PRODUCTS.ID + BR + COMMA + TRANSACTION_ID + SPACE + 
			SQLStatements.INT + SPACE + SQLStatements.REFERENCES + SPACE + 
			TRANSACTION_TABLE + SPACE + BL + Schema.TRANSACTION.ID + BR + BR;
	}
	
	public interface WORKS {
		
		public static final String EMPLOYEE = "employee_id";
		public static final String TIMETABLE = "shift_id";
		
		public static final String CREATE_TABLE = 
			SQLStatements.CREATE + SPACE + SQLStatements.TABLE + SPACE +
			SQLStatements.IFNE + SPACE + WORKS_TABLE + SPACE + BL +
			EMPLOYEE + SPACE + SQLStatements.INT + SPACE + 
			SQLStatements.NOT_NULL + COMMA + TIMETABLE + SPACE + 
			SQLStatements.INT + SPACE + SQLStatements.NOT_NULL + COMMA +
			SQLStatements.FOREIGN_KEY + SPACE + BL + EMPLOYEE + BR + SPACE + 
			SQLStatements.REFERENCES + SPACE + EMPLOYEE_TABLE + SPACE + BL
			+ Schema.EMPLOYEE.ID + BR + SPACE + COMMA + SQLStatements.FOREIGN_KEY +
			SPACE + BL + TIMETABLE + BR + SPACE + SQLStatements.REFERENCES + 
			SPACE + TIMETABLE_TABLE + SPACE + BL + Schema.TIMETABLE.SHIFT_ID 
			+ BR + COMMA + SQLStatements.PRIMARYKEY + BL + EMPLOYEE + COMMA +
			TIMETABLE + BR + BR;
	}
	
	public interface DELIVERIES {
		
		public static final String DRIVER = "driver_id";
		public static final String TRANSACTION = "transaction_id";
		
		public static final String CREATE_TABLE = 
			SQLStatements.CREATE + SPACE + SQLStatements.TABLE + SPACE +
			SQLStatements.IFNE + SPACE + DELIVERIES_TABLE + SPACE + BL + 
			DRIVER + SPACE + SQLStatements.INT + SPACE + SQLStatements.NOT_NULL 
			+ SPACE + SQLStatements.REFERENCES + SPACE + DRIVER_TABLE + SPACE + 
			BL + Schema.DRIVER.EMPLOYEE_ID + BR + COMMA + TRANSACTION + SPACE + 
			SQLStatements.INT + SPACE + SQLStatements.NOT_NULL + SPACE +
			SQLStatements.REFERENCES + SPACE + TRANSACTION_TABLE + SPACE + 
			BL + Schema.TRANSACTION.ID + BR + COMMA + SQLStatements.PRIMARYKEY +
			BL + DRIVER + COMMA + TRANSACTION + BR + BR;
	}
	
	public interface HAS_CAR {
		
		public static final String VICHELE = "vichele_id";
		public static final String DRIVER = "driver_id";
		
		public static final String CREATE_TABLE = 
			SQLStatements.CREATE + SPACE + SQLStatements.TABLE + SPACE +
			SQLStatements.IFNE + SPACE + HAS_CAR_TABLE + SPACE + BL + 
			VICHELE + SPACE + SQLStatements.VARCHAR + BL + "10" + BR 
			+ SPACE + SQLStatements.NOT_NULL
			+ SPACE + SQLStatements.REFERENCES + SPACE + VICHELE_TABLE + SPACE +
			BL + Schema.VICHELE.LICENSE + BR + COMMA + DRIVER + SPACE + 
			SQLStatements.INT + SPACE + SQLStatements.NOT_NULL + SPACE + 
			SQLStatements.REFERENCES + SPACE + DRIVER_TABLE + SPACE + BL +
			Schema.DRIVER.EMPLOYEE_ID + BR + COMMA + SQLStatements.PRIMARYKEY +
			BL + VICHELE + COMMA + DRIVER + BR + BR;
	}
}
