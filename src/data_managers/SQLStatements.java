package data_managers;

/**
 * Inteface to handle all SQL statement clauses 
 * @author saif
 *
 */
public interface SQLStatements {
	
	public static final String SPACE = " ";
	public static final String BR = ")";
	public static final String BL = "(";
	public static final String COMMA = ",";
	public static final String EQ = "=";
	
	//MAIN CLAUSES
	public static final String SELECT = "SELECT";
	public static final String FROM = "FROM";
	public static final String WHERE = "WHERE";
	
	public static final String NOT = "NOT";
	public static final String IN = "IN";
	public static final String NOTIN = NOT + " " + IN;
	public static final String AND = "AND";
	public static final String ANY = "ANY";
	public static final String EXISTS = "EXISTS";
	public static final String ON = "ON";
	public static final String IF = "IF";
	public static final String IFNE = IF + SPACE + NOT + SPACE + EXISTS;
	public static final String AS = "AS";
	public static final String OR = "OR";
	
	//GROUPING 
	public static final String GROUP = "GROUP BY";
	public static final String HAVING = "HAVING";
	
	//AGGREGATE OPERATORS
	public static final String SUM = "SUM";
	public static final String AVG = "AVG";
	public static final String COUNT = "COUNT";
	public static final String COUNTROWS = "COUNT(*)";
	public static final String MIN = "MIN";
	public static final String MAX = "MAX";
	public static final String DATEDIFF = "DATEDIFF";
	
	//SQL TYPES
	public static final String INT = "INT";
	public static final String TEXT = "TEXT";
	public static final String CHAR = "CHAR";
	public static final String VARCHAR = "VARCHAR";
	public static final String BOOLEAN = "BOOLEAN";
	public static final String FLOAT = "FLOAT";
	public static final String REAL = "REAL";
	public static final String DATE = "DATE";
	public static final String TIME = "TIME";
	public static final String CURTIME = "CURTIME" + BL + BR;
	public static final String NULL = "NULL";
	
	
	//KEY TYPES
	public static final String UNIQUE = "UNIQUE";
	public static final String KEY = "KEY";
	public static final String PRIMARYKEY = "PRIMARY " + KEY;

	
	//SET OPPERATORS
	public static final String UNION = "UNION";
	public static final String INTERSECT = "INTERSECT";
	public static final String JOIN = "JOIN";
	public static final String NATURALJOIN = "NATURAL JOIN";
	public static final String LEFT = "LEFT";
	public static final String RIGHT = "RIGHT";
	public static final String DISTINCT = "DISTINCT";
	public static final String CROSSJOIN = "CROSS JOIN";
	public static final String OUTERJOIN = "OUTER JOIN";
	public static final String EXCEPT = "EXCEPT";
	
	//STRING CLAUSES
	public static final String LIKE = "LIKE";
	public static final String WILDSTRING = "%";
	public static final String WILDCARD = "?";
	
	//LOGICAL OPERATORS
	public static final String GREATER = ">";
	public static final String LESS = "<";
	public static final String EQUALS = "=";
	
	//DATABASE OPERATORS
	public static final String CREATE = "CREATE";
	public static final String DATABASE = "DATABASE";
	public static final String TABLE = "TABLE";
	public static final String FOREIGN = "FOREIGN";
	public static final String FOREIGN_KEY = FOREIGN + " " + KEY;
	public static final String REFERENCES = "REFERENCES";
	public static final String TRIGGER = "TRIGGER";
	public static final String ASSERTION = "ASSERTION";
	public static final String CHECK = "CHECK";
	public static final String UPDATE = "UPDATE";
	public static final String SET = "SET";
	public static final String EACH = "EACH";
	public static final String ROW = "ROW";
	public static final String OLD_ROW = "OLD " + ROW;
	public static final String NEW_ROW = "NEW " + ROW;
	public static final String VIEW = "VIEW";
	public static final String AFTER = "AFTER";
	public static final String ALTER = "ALTER";
	public static final String CASCADE = "CASCADE";
	public static final String SET_NULL = SET + " NULL";
	public static final String DELETE = "DELETE";
	public static final String NOT_NULL = NOT + " NULL";
	public static final String AUTO_INCREMENT = "AUTO_INCREMENT";
	public static final String DEFAULT = "DEFAULT";
	public static final String INSERT = "INSERT";
	public static final String INTO = "INTO";
	public static final String VALUES = "VALUES";
	public static final String LAST_INSERT_ID = "last_insert_id()";
	public static final String LOAD = "LOAD";
	public static final String DATA = "DATA";
	public static final String INFILE = "INFILE";
	public static final String LOCAL = "LOCAL";
	public static final String FIELDS = "FIELDS";
	public static final String TERMINATEDBY = "TERMINATED BY";
}
