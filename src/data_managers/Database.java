package data_managers;

import java.sql.*;


import data_managers.Schema;

public class Database {
	
	public static String[] tableExecutions = {
		Schema.CUSTOMER.CREATE_TABLE, Schema.TRANSACTION.CREATE_TABLE, 
		Schema.PRODUCTS.CREATE_TABLE, Schema.TIMETABLE.CREATE_TABLE,
		Schema.ADDRESS.CREATE_TABLE, Schema.VICHELE.CREATE_TABLE,
		Schema.EMPLOYEE.CREATE_TABLE, Schema.DRIVER.CREAT_TABLE,
		Schema.EMPLOYEE_ADDRESS.CREATE_TABLE, 
		Schema.CUSTOMER_ADDRESS.CREATE_TABLE, 
		Schema.PAYMENT_METHOD.CREATE_TABLE, Schema.CREDIT_CARD.CREATE_TABLE,
		Schema.CHEQUE.CREATE_TABLE, Schema.CASH.CREATE_TABLE, 
		Schema.ORDERS.CREATE_TABLE, Schema.ORDER_ITEMS.CREATE_TABLE,
		Schema.HAS_PAYMENT.CREATE_TABLE, Schema.HAS_CAR.CREATE_TABLE,
		Schema.WORKS.CREATE_TABLE, Schema.DELIVERIES.CREATE_TABLE};

	public static int createDatabase(Connection connection) {
		
		int executionResult = -1;
		try {
		
			Statement createDb = connection.createStatement();
			String createDbStatement = SQLStatements.CREATE + 
					SQLStatements.SPACE + SQLStatements.DATABASE +
					SQLStatements.SPACE + Schema.DATABASE_NAME;
			executionResult = createDb.executeUpdate(createDbStatement);
			executionResult = 1;
		}
		
		catch (SQLException e) {
			System.err.println(e.toString());
			executionResult = -1;
		}
		
		return executionResult;
	}
	
	public static int createTables(Connection connection) {
		
		int executionResult = 1;
		
		try {
			System.out.println("Creating Tables");
			Statement createTables = connection.createStatement();
			
			for (int i = 0; i < tableExecutions.length; i++) {
				
				String call = tableExecutions[i];
				System.out.println(call);
				createTables.executeUpdate(call);
			}
		}
		
		catch (SQLException e) {
			System.err.println(e.toString());
			executionResult = -1;
		}
		
		return executionResult;
	}
}
